package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.entity.Movie;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.newitventure.mobile.GlobalHimalayan.R;


import java.util.ArrayList;


public class MoviesHorizontalListAdapter extends ArrayAdapter<Movie> {

    private Context context;
    private int resourceId;
    private ArrayList<Movie> movieList;

    public MoviesHorizontalListAdapter(Context context,
                                       int textViewResourceId, ArrayList<Movie> objects) {
        super(context, textViewResourceId, objects);


        this.context = context;
        this.resourceId = textViewResourceId;
        this.movieList = objects;
    }

    @Override
    public int getCount() {

        return movieList.size();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resourceId, parent, false);

            holder = new Holder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Movie currentMovie = movieList.get(position);

        holder.textView.setText(currentMovie.getMovieName());

        try {
            UrlImageViewHelper.setUrlDrawable(holder.imageView, currentMovie.getMoviePictureLinkString(), R.drawable.placeholder_logo);
        } catch (Exception e) {
            holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder_logo));
        }




        Log.d("ADAPTER", holder.textView.getText().toString());
        return convertView;
    }

    private class Holder {
        private TextView textView;
        private ImageView imageView;
    }

}
