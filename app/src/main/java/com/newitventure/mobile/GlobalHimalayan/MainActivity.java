package com.newitventure.mobile.GlobalHimalayan;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.newitventure.mobile.GlobalHimalayan.adapter.myAdapter;
import com.newitventure.mobile.GlobalHimalayan.fragment.AccountFragment;
import com.newitventure.mobile.GlobalHimalayan.fragment.LiveFragment;
import com.newitventure.mobile.GlobalHimalayan.fragment.MoviesFragment;
import com.newitventure.mobile.GlobalHimalayan.fragment.TvshowsFragment;
import com.newitventure.mobile.GlobalHimalayan.fragment.YoutubePlusFragment;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;
import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    Toolbar mtoolbar;
    DrawerLayout mDrawer;
    ActionBarDrawerToggle mDrawerToggle;
    LiveFragment liveFragment;
    public static SharedPreferences app_preferences;
    private static final String TAG = "LoginActivity";
    Boolean islogin, isdflogin;
    String email;
    String link;
    ImageView image;
    TextView txtemail;
    String TITLES[] = {"Live TV", "YouTube Plus", "Movies", "Tv Shows", "My Account", "Settings"};
    int ICONS[] = {R.drawable.live_tv, R.drawable.youtube_plus, R.drawable.movies, R.drawable.tv_shows, R.drawable.my_account, R.drawable.settings};
    RecyclerView recyclerView;
    AlarmManager am;
    PendingIntent pi;
    BroadcastReceiver broadcastDoubleLoginCheck;
    protected static final long SESSION_RESET_TIME = 24 * 60 * 60 * 1000;//1 day
    long multiLoginCheckInterval;
    Toolbar toolbar;
    NavigationView navigationView;

    String macAddress;
    String userid, sessionid, useremail;
    String platform = "mobile";
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        tvshowsAllContent = new HashMap<String, ArrayList<HashMap<String, String>>>();

        setContentView(R.layout.activity_main);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        liveFragment = new LiveFragment();
        ft.add(R.id.content_frame, liveFragment);
        ft.commit();
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2941478610557201/9438902220");
        requestNewInterstitial();

        app_preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        userid = app_preferences.getString("userid", "");
        sessionid = app_preferences.getString("sessionid", "");
        useremail = app_preferences.getString("useremail", "");

        setupMultipleLogin();
        multiLoginCheckInterval = 60 * 1000;
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), multiLoginCheckInterval, pi);

        mtoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setTitle(" ");

        image = (ImageView) findViewById(R.id.circleView);
        txtemail = (TextView) findViewById(R.id.email);

        mDrawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawer, mtoolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawer.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                menuItem.setChecked(true);
                int id = menuItem.getItemId();

                switch (id) {

                    case R.id.liveTv:

                        liveFragment = new LiveFragment();
                        ft.replace(R.id.content_frame, liveFragment);
                        ft.commit();

                        break;

                    case R.id.youtubePlus:

                        YoutubePlusFragment youtubeFragment = new YoutubePlusFragment();
                        ft.replace(R.id.content_frame, youtubeFragment);
                        ft.commit();

                        break;

                    case R.id.movies:

                        MoviesFragment moviesFragment = new MoviesFragment();
                        ft.replace(R.id.content_frame, moviesFragment);
                        ft.commit();

                        break;

                    case R.id.tvShows:

                        TvshowsFragment tvshowsFragment = new TvshowsFragment();
                        ft.replace(R.id.content_frame, tvshowsFragment);
                        ft.commit();

                        break;


                    case R.id.myAccount:

                        AccountFragment accountFragment = new AccountFragment();
                        ft.replace(R.id.content_frame, accountFragment);
                        ft.commit();

                        break;

                    case R.id.settings:

                        Intent i = new Intent(MainActivity.this, Settings.class);
                        startActivity(i);

                        break;

                }

                mDrawer.closeDrawers();

                return false;

            }

        });

        app_preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        islogin = app_preferences.getBoolean("fbsignedin", false);
        isdflogin = app_preferences.getBoolean("opsignedin", false);

        if (islogin == true) {
            loadData();

        } else if (isdflogin == true) {
            loaddatadf();
        }

    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(GravityCompat.START)) {

            mDrawer.closeDrawer(GravityCompat.START);

        }


        /**
         * checks whether the current fragment in the container is the Live fragment or not
         */
        else if (getSupportFragmentManager().findFragmentById(R.id.content_frame) instanceof LiveFragment) {

            if (mInterstitialAd.isLoaded()) {

                mInterstitialAd.show();

                super.onBackPressed();

            }

        } else {

            LiveFragment fragment = new LiveFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            handleMenuItemSelection();

        }

    }

    private void handleMenuItemSelection() {

        navigationView.getMenu().getItem(0).setChecked(true);

    }

    private void setupMultipleLogin() {

        if (NetworkUtil.isInternetAvailable(MainActivity.this)) {


            broadcastDoubleLoginCheck = new BroadcastReceiver() {
                @Override
                public void onReceive(Context c, Intent i) {
                    long millis = 1 * 24 * 60 * 60 * 1000; //1 day
                    long currentMillis = Calendar.getInstance().getTimeInMillis();


                    Log.e("Runs frequently: Session ID at MovieCategories:", sessionid + "");

                    String link = LinkConfig.getString(MainActivity.this, LinkConfig.MULTI_LOGIN) + "?" +
                            "session=" + sessionid +
                            "&name=" + useremail;


                    new MultipleLoginCheckTask(MainActivity.this, broadcastDoubleLoginCheck).execute(link);
                }
            };
            registerReceiver(broadcastDoubleLoginCheck, new IntentFilter("com.nitv.wodlivechannels.sessionService"));

            pi = PendingIntent.getBroadcast(this, 0, new Intent("com.nitv.wodlivechannels.sessionService"), 0);

            am = (AlarmManager) (this.getSystemService(Context.ALARM_SERVICE));

        }

    }

    private void loaddatadf() {

        String username = app_preferences.getString("useremail", "");
        txtemail.setText(username);

    }

    private void loadData() {
        email = app_preferences.getString("fbemail", "");
        Log.v(TAG, "THE fbemail is " + email);
        link = app_preferences.getString("link", "");
        Log.v(TAG, "onresume is called" + link);

        Picasso.with(this)
                .load(link)
                .into(image);
        txtemail.setText(email);
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    private HashMap<String, ArrayList<HashMap<String, String>>> tvshowsAllContent;

    public void setTvShowContent(String key, ArrayList<HashMap<String, String>> content) {

        tvshowsAllContent.put(key, content);

    }

    public ArrayList<HashMap<String, String>> getTvShowContent(String key) {

        if (tvshowsAllContent.containsKey(key)) {
            return tvshowsAllContent.get(key);
        }

        return null;
    }

}
