package com.newitventure.mobile.GlobalHimalayan.entity;

import android.os.Parcel;
import android.os.Parcelable;


public class Movie implements Parcelable {
	private int movieId;
	private String movieName;
	private String movieDescription;
	private int movieCategoryParentId;
	private int movieCategoryId;
	private String movieCategoryTitle;
	private String movieUrlString;
	private int movieServerType;
	private String moviePictureLinkString;
	private int movieStatus;
	private double moviePrice;

	public Movie(Parcel source) {
		
		movieId = source.readInt();
		movieName = source.readString();
		movieDescription = source.readString();
		movieCategoryParentId = source.readInt();
		movieCategoryId = source.readInt();
		movieCategoryTitle = source.readString();
		movieUrlString = source.readString();
		movieServerType = source.readInt();
		moviePictureLinkString = source.readString();
		movieStatus=source.readInt();
		moviePrice = source.readDouble();
	}

	
	public Movie(){}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(movieId);
		dest.writeString(movieDescription);
		dest.writeInt(movieCategoryParentId);
		dest.writeInt(movieCategoryId);
		dest.writeString(movieCategoryTitle);
		dest.writeString(movieUrlString);
		dest.writeInt(movieServerType);
		dest.writeString(moviePictureLinkString);
		dest.writeInt(movieStatus);
		dest.writeDouble(moviePrice);
		
	}
	
	public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        public Movie createFromParcel(Parcel source) {
            return new Movie(source); 
        }

		@Override
		public Movie[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Movie[size];
		}
	};
	
	public void setMovieId( int movieId ) {
		this.movieId = movieId;
	}
	
	public void setMovieName( String movieName ) {
		this.movieName = movieName;
	}
	
	public void setMovieDescription( String movieDescription ) {
		this.movieDescription = movieDescription;
	}
	
	public void setMovieCategoryParentId( int movieCategoryParentId ) {
		this.movieCategoryParentId = movieCategoryParentId;
	}
	
	public void setMovieCategoryId( int movieCategoryId ) {
		this.movieCategoryId = movieCategoryId;
	}
	
	public void setMovieCategoryTitle( String movieCategoryTitle ) {
		this.movieCategoryTitle = movieCategoryTitle;
	}
	
	public void setMovieUrlString( String movieUrlString) {
		this.movieUrlString = movieUrlString;
	}
	
	public void setMovieServerType( int movieServerType) {
		this.movieServerType = movieServerType;
	}
	
	public void setMoviePictureLinkString( String moviePictureLinkString ) {
		this.moviePictureLinkString = moviePictureLinkString;
	}
	
	public void setMovieStatus( int movieStatus ) {
		this.movieStatus = movieStatus;
	}
	
	public void setMoviePrice( double moviePrice ) {
		this.moviePrice = moviePrice;
	}
	
	
	public int getMovieId() {
		return movieId;
	}
	
	public String getMovieName() {
		return movieName;
	}
	
	public String getMovieDescription() {
		return movieDescription;
	}
	
	public int getMovieCategoryParentId() {
		return movieCategoryParentId;
	}
	
	public int getMovieCategoryId() {
		return movieCategoryId;
	}
	
	public String getMovieCategoryTitle() {
		return movieCategoryTitle;
	}
	
	public String getMovieUrlString() {
		return movieUrlString;
	}
	
	public int getMovieServerType() {
		return movieServerType;
	}
	
	public String getMoviePictureLinkString() {
		return moviePictureLinkString;
	}
	
	public int getMovieStatus() {
		return movieStatus;
	}
	
	public double getMoviePrice() {
		return moviePrice;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append( "Movie[" )
			.append( "\n\tID " ).append( movieId )
			.append( "\n\tName " ).append( movieName )
			.append( "\n\tDesc " ).append( movieDescription )
			.append( "\n\tParent ID " ).append( movieCategoryParentId )
			.append( "\n\tCategory ID " ).append( movieCategoryId )
			.append( "\n\tCategory Name " ).append( movieCategoryTitle )
			.append( "\n\tURL " ).append( movieUrlString )
			.append( "\n\tServer Type " ).append( movieServerType )
			.append( "\n\tBanner " ).append( moviePictureLinkString )
			.append( "\n\tStatus " ).append( movieStatus )
			.append( "\tPrice " ).append( moviePrice )
			.append( "\n]");
			
		return sb.toString();
	}


	public String getMovieInString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();

		sb.append(movieId).append("\n").append(movieName).append("\n")
				.append(movieDescription).append("\n")
				.append(movieCategoryParentId).append("\n")
				.append(movieCategoryId).append("\n")
				.append(movieCategoryTitle).append("\n").append(movieUrlString)
				.append("\n").append(movieServerType).append("\n")
				.append(moviePictureLinkString).append("\n")
				.append(movieStatus).append("\n").append(moviePrice)
				.append("\n");
		return sb.toString();
	}

	

	
}
