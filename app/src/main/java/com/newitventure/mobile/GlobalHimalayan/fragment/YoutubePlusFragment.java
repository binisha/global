package com.newitventure.mobile.GlobalHimalayan.fragment;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.adapter.ViewPagerAdapterYoutubePlus;
import com.newitventure.mobile.GlobalHimalayan.adapter.YouTubeCategoriesAdapter;
import com.newitventure.mobile.GlobalHimalayan.parser.JSONParser;
import com.newitventure.mobile.GlobalHimalayan.parser.YoutubeCategoriesParentParser;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.CustomViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link YoutubePlusFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class YoutubePlusFragment extends Fragment {


    private CustomViewPager pager;
    private SlidingTabLayout slidingTabs;
    ViewPagerAdapterYoutubePlus adapter;
    public RelativeLayout relativeLayout;
    JSONArray categories = null;
    private ArrayList<YouTubeCategoriesAdapter> youtubeCatList;
    TextView textView;
    private static final String BASE_URL_JSON = "http://internationaltv.sabhkuchh.com/mobile/youtube/youtubechannel_json.php";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param
     * @return A new instance of fragment YoutubePlusFragment.
     */
    // TODO: Rename and change types and number of parameters
    public YoutubePlusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_youtube_plus, container, false);
        pager = (CustomViewPager) v.findViewById(R.id.youtube_viewpager);
        pager.setPagingEnabled(true); //set false to disable swipe
        slidingTabs = (SlidingTabLayout) v.findViewById(R.id.youtube_Sliding_Tabs);
        textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
        textView.setText("Youtube Plus");
        relativeLayout = (RelativeLayout) v.findViewById(R.id.relative_layout);

        new YoutubeCategoryJsonParser().execute(BASE_URL_JSON);
        return v;
    }

    public void onButtonPressed(Uri uri) {

        if (mListener != null) {
            mListener.onFragmentInteraction(uri);

        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private class YoutubeCategoryJsonParser extends AsyncTask<String, Void, JSONArray> {

        @Override
        protected void onPreExecute() {

            relativeLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONArray doInBackground(String... params) {

            JSONParser jParser = new JSONParser();
            JSONObject json = jParser.getJSONFromUrl(BASE_URL_JSON);

            try {
                categories = json.getJSONArray("youtube_channel");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return categories;
        }

        protected void onPostExecute(JSONArray result) {

            try {

                if (result != null) {

                    relativeLayout.setVisibility(View.GONE);

                    YoutubeCategoriesParentParser parser = new YoutubeCategoriesParentParser(
                            result, getActivity());
                    parser.parse();
                    youtubeCatList = parser.getMovieCategoryParentList();

                    adapter = new ViewPagerAdapterYoutubePlus(getChildFragmentManager(), youtubeCatList, youtubeCatList.size());
                    pager.setAdapter(adapter);

                    slidingTabs.setDistributeEvenly(true);
                    slidingTabs.setBackgroundColor(getResources().getColor(R.color.primary));
                    slidingTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
                    slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));
                    slidingTabs.setViewPager(pager);

                }

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }

}
