package com.newitventure.mobile.GlobalHimalayan.Tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.newitventure.mobile.GlobalHimalayan.MainActivity;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.TvShowPlayActivity;
import com.newitventure.mobile.GlobalHimalayan.adapter.YoutubePlaylist;
import com.newitventure.mobile.GlobalHimalayan.entity.YoutubePlaylistObj;
import com.newitventure.mobile.GlobalHimalayan.parser.JSONParser;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.google.android.gms.internal.zzip.runOnUiThread;


/**
 * Created by NITV on 8/6/2015.
 */
public class TvShowsTab extends Fragment {


    private SharedPreferences app_preferences;
    View v;
    private ListView listView;
    int position, catIdVOD;
    //private static String GOOGLE_API_KEY = "AIzaSyBYkoGH-gYcpICI1n1JNToRmC_7DUYo8Rs";
    private static String GOOGLE_API_KEY = "AIzaSyCuO_7-soNoGNbbc9lSJI1pdLPx8Znt0K0";//this one is the browser key
    private static final String PLAYLIST_NO = "playlist_number";
    private static final String TAG_CATEGORY = "subcategory";
    private static final String TAG_CATEGORY_ID = "category_id";
    private static final String TAG_CATEGORY_TITLE = "category_title";
    private static final String TAG_CATEGORY_THUMBNAIL = "thumbnails";
    public ProgressBar progressBar;
    JSONArray categories;
    private static ArrayList<HashMap<String, String>> categoryArrayList;
    ArrayList<YoutubePlaylistObj> youtubePlaylistArray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        position = bundle.getInt("page_position");
        catIdVOD = bundle.getInt("catId");

        v = inflater.inflate(R.layout.youtube_tab, container, false);// the same layout is used because both YOUTUBEPLUS AND VOD requires simlar attriutes

        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);

        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        userid = app_preferences.getString("userid", "");
        listView = (ListView) v.findViewById(R.id.youTubeList);

        new AsyncTaskToFetchJson().execute();

        return v;

    }

    public class AsyncTaskToFetchJson extends AsyncTask<String, String, String> {

        String userId;

        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);

        }

        protected void onPostExecute(String result) {

            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected String doInBackground(String... arg0) {

            categoryFeed(catIdVOD + "");

            return null;

        }

    }

    public void categoryFeed(final String catIdVOD) {

        categoryArrayList = new ArrayList<HashMap<String, String>>();
        youtubePlaylistArray = new ArrayList<YoutubePlaylistObj>();

        try {

            JSONParser jsonParser = new JSONParser();
            JSONObject json = jsonParser.getJSONFromUrl(LinkConfig.getString(
                    getActivity(), LinkConfig.YOUTUBE_PLAYLIST)
                    + "?catID=" + catIdVOD);
            Log.d("PLAYLIST",
                    LinkConfig.getString(getActivity(),
                            LinkConfig.YOUTUBE_PLAYLIST) + "?catID=" + catIdVOD);

            categories = json.getJSONArray(TAG_CATEGORY);

            for (int i = 0; i < categories.length(); i++) {

                JSONObject jO = categories.getJSONObject(i);
                String catId = jO.getString(TAG_CATEGORY_ID);
                String catTitle = jO.getString(TAG_CATEGORY_TITLE);
                String playListNo = jO.getString(PLAYLIST_NO);

                HashMap<String, String> map = new HashMap<String, String>();
                map.put(TAG_CATEGORY_ID, catId);
                map.put(TAG_CATEGORY_TITLE, catTitle);
                map.put(PLAYLIST_NO, playListNo);
                categoryArrayList.add(map);

            }

            ((MainActivity) getActivity()).setTvShowContent(catIdVOD, categoryArrayList);

            final ListAdapter adapter = new SimpleAdapter(
                    getActivity(),
                    categoryArrayList,
                    R.layout.list_item_vod,
                    new String[]{TAG_CATEGORY_ID, TAG_CATEGORY_TITLE},
                    new int[]{R.id.category_id, R.id.category_title});
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Log.e("category check", categoryArrayList + "");
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ArrayList<HashMap<String, String>> temp = ((MainActivity) getActivity()).getTvShowContent(catIdVOD);

                            Intent i = new Intent(getActivity(), TvShowPlayActivity.class);
                            i.putExtra("playlist_id", temp.get(position).get(PLAYLIST_NO));
                            i.putExtra("Category_title", temp.get(position).get(TAG_CATEGORY_TITLE));
                            i.putExtra("category_size", temp.size());
                            startActivity(i);

                        }
                    });

                }
            });

        } catch (JSONException e) {

            Log.e("exception", "JSONException 626: " + e.getMessage());
            e.printStackTrace();
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Toast.makeText(getActivity(), "No Data Found",
                            Toast.LENGTH_LONG).show();
                }

            });

        } catch (Exception e) {

            e.printStackTrace();
            Log.e("exception", "Exception 631: " + e.getMessage());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {

                        Toast.makeText(getActivity(), "No Data Found",
                                Toast.LENGTH_LONG).show();

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            });

        }

    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

    }

}
