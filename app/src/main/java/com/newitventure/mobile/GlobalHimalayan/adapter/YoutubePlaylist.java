package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.YoutubePlaylistObj;

import java.util.ArrayList;

/**
 * Created by rohit on 12/1/15.
 */
public class YoutubePlaylist extends ArrayAdapter<YoutubePlaylistObj> {

    private final Activity context;
    private ArrayList<YoutubePlaylistObj> youtubePlaylistobj;


    public YoutubePlaylist(Activity context, ArrayList<YoutubePlaylistObj> youtubePlaylistObj) {

        super(context, R.layout.youtube_playlist_single_item, youtubePlaylistObj);
        this.context = context;
        this.youtubePlaylistobj = youtubePlaylistObj;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.youtube_playlist_single_item, null, true);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.youtube_thumbnail);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.youtube_title);
        txtTitle.setText(youtubePlaylistobj.get(position).getTitle());

        if (youtubePlaylistobj.get(position).getThumbnail() == null) {

            imageView.setVisibility(View.GONE);

        } else {

            UrlImageViewHelper.setUrlDrawable(imageView, youtubePlaylistobj.get(position).getThumbnail(),
                    R.drawable.placeholder_logo, 2 * 1000 * 60);

        }

        return rowView;

    }

    @Override
    public int getPosition(YoutubePlaylistObj item) {

        return super.getPosition(item);

    }

}