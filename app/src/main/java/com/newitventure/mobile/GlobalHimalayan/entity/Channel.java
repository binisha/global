package com.newitventure.mobile.GlobalHimalayan.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Channel implements Parcelable{

    private int channelId;
    private String channelName;
    private double channelPrice;
    private int channelServerType;
    private int channelCdnServerType;
    private String channelDescription;
    private String channelLanguage;
    private int channelPriority;
    private String channelUrl;
    private String channelLogoLink;
    private int userId;
    private int favourite;
    private String channelCountry;

    public String getChannelCountry() {
        return channelCountry;
    }



    public void setChannelCountry(String channelCountry) {
        this.channelCountry = channelCountry;
    }


    public Channel(Parcel source) {

        channelId = source.readInt();
        channelName = source.readString();
        channelLanguage = source.readString();
        channelDescription = source.readString();
        channelLogoLink = source.readString();
        channelPriority = source.readInt();
        channelCountry = source.readString();
        favourite = source.readInt();
    }



    public Channel() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeInt(channelId);
        dest.writeString(channelName);
        dest.writeString(channelLanguage);
        dest.writeString(channelDescription);
        dest.writeString(channelLogoLink);
        dest.writeInt(channelPriority);
        dest.writeString(channelCountry);
        dest.writeInt(favourite);
    }

    public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
        public Channel createFromParcel(Parcel source) {
            return new Channel(source);
        }

        @Override
        public Channel[] newArray(int size) {
            // TODO Auto-generated method stub
            return new Channel[size];
        }
    };

    public void setChannelId( int channelId ) {
        this.channelId = channelId;
    }

    public void setChannelName( String channelName ) {
        this.channelName = channelName;
    }

    public void setChannelPrice( double channelPrice ) {
        this.channelPrice = channelPrice;
    }

    public void setChannelServerType( int channelServerType ) {
        this.channelServerType = channelServerType;
    }

    public void setChannelCdnServerType( int channelCdnServerType ) {
        this.channelCdnServerType = channelCdnServerType;
    }

    public void setChannelDescription( String channelDescription ) {
        this.channelDescription = channelDescription;
    }

    public void setChannelLanguage( String channelLanguage ) {
        this.channelLanguage = channelLanguage;
    }

    public void setChannelPriority( int channelPriority ) {
        this.channelPriority = channelPriority;
    }

    public void setChannelUrl( String channelUrl ) {
        this.channelUrl = channelUrl;
    }

    public void setChannelLogoLink( String channelLogoLink ) {
        this.channelLogoLink = channelLogoLink;
    }

    public void setUserId( int userId ) {
        this.userId = userId;
    }

    public void setFavourite( int favourite ) {
        this.favourite = favourite;
    }

    public int getChannelId() {
        Log.d("the channel id is", String.valueOf(channelId));
        return this.channelId;
    }

    public String getChannelName() {
        return this.channelName;
    }

    public double channelPrice() {
        return this.channelPrice;
    }

    public int getChannelServerType() {
        return this.channelServerType;
    }

    public int getChannelCdnServerType() {
        return this.channelCdnServerType;
    }

    public String getChannelDescription() {
        return this.channelDescription;
    }

    public String getChannelLanguage() {
        return this.channelLanguage;
    }

    public int getChannelPriority() {
        return this.channelPriority;
    }

    public String getChannelUrl() {
        return this.channelUrl;
    }

    public String getChannelLogoLink() {
        return "http://" + this.channelLogoLink;
    }

    public int getUserId() {
        return this.userId;
    }

    public int getFavourite() {

        return this.favourite;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append( "Channel[" )
                .append( "\n\t Channel ID: ").append( this.channelId )
                .append( "\n\t Channel Name: ").append( this.channelName )
                .append( "\n\t Channel Server Type: ").append( this.channelServerType )
                .append( "\n\t Channel CDN Server Type: ").append( this.channelCdnServerType )
                .append( "\n\t Channel Description: ").append( this.channelDescription )
                .append( "\n\t Channel Language: ").append( this.channelLanguage )
                .append( "\n\t Channel Priority: ").append( this.channelPriority )
                .append( "\n\t Channel Url: ").append( this.channelUrl )
                .append( "\n\t Channel Price: ").append( this.channelPrice )
                .append( "\n\t Channel Logo Link: ").append( this.channelLogoLink )
                .append( "\n\t Channel User: ").append( this.userId )
                .append( "\n\t Channel Fav: ").append( this.favourite )
                .append( "\n\t Channel Country: ").append(this.channelCountry)
                .append( "\n]" );

        return sb.toString();
    }

}
