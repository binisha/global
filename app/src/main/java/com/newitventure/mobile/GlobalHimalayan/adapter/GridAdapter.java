package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.newitventure.mobile.GlobalHimalayan.entity.Item;
import com.newitventure.mobile.GlobalHimalayan.R;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    private Activity context;
    private ArrayList<Item> itemList;
    private String[] names;
    private int GridView;
    private int gridviewLayout;

    public GridAdapter(Activity context, ArrayList<Item> itemList,
                       int gridviewLayout) {
        super();
        this.context = context;
        this.itemList = itemList;
        this.gridviewLayout = gridviewLayout;

    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.gridview_layout, null, true);
        Item item = itemList.get(i);

        ImageView image = (ImageView) rowView.findViewById(R.id.image);

        TextView title = (TextView) rowView.findViewById(R.id.title);
        image.setImageDrawable(context.getResources()
                .getDrawable(item.getRes()));
        title.setText(item.getTitle());
        return rowView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}