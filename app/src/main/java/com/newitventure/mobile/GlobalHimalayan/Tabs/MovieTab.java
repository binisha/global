package com.newitventure.mobile.GlobalHimalayan.Tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.newitventure.mobile.GlobalHimalayan.NoInternetActivity;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.adapter.MoviesListAdapter;
import com.newitventure.mobile.GlobalHimalayan.entity.Movie;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategory;
import com.newitventure.mobile.GlobalHimalayan.fragment.MoviesFragment;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;
import com.newitventure.mobile.GlobalHimalayan.parser.MovieCategoryParser;
import com.newitventure.mobile.GlobalHimalayan.parser.MovieParser;
import com.newitventure.mobile.GlobalHimalayan.util.UnauthorizedAccess;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by NITV on 8/6/2015.
 */
public class MovieTab extends Fragment {
    private static final String TAG = "com.example.dell.WodApplication.Tabs'.Movies";


    private GridView gv;
    private TextView txtMovieTitle, txtMovieDescription;
    private SharedPreferences app_preferences;
    public static String userid, sessionid, userGroup;
    String moviesContentLinkString, moviesCategoryLinkString;
    View v;
    public ProgressBar progressBar;
    int position;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        position = bundle.getInt("page_position");

        loadJsonTab(inflater, container);

        return v;

    }


    public void loadJsonTab(LayoutInflater inflater, ViewGroup container) {

        v = inflater.inflate(R.layout.movies_tab, container, false);
        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");
        sessionid = app_preferences.getString("sessionid", "");
        userGroup = app_preferences.getString("user_group", "");

        gv = (GridView) v.findViewById(R.id.movieList);

        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);

        try {

            moviesContentLinkString = LinkConfig.getString(
                    getActivity(),
                    LinkConfig.MOVIE_CATEGORY) + "?catID=";
            moviesCategoryLinkString = LinkConfig.getString(getActivity(),
                    LinkConfig.MOVIE_CATEGORY_BK) + "?catID=";
            moviesContentLinkString += MoviesFragment.movieCategoryParentList.get(position)
                    .getParentId() + "&userId=" + userid + "&";
            moviesCategoryLinkString += MoviesFragment.movieCategoryParentList.get(position)
                    .getParentId() + "&userId=" + userid + "&";

            new MoviesCategoryLoader().execute(moviesCategoryLinkString);

        } catch (Exception e) {

            e.printStackTrace();

        }

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    private class MoviesCategoryLoader extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... params) {

            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, getActivity());
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {

                DownloadUtil dUtil = new DownloadUtil(params[0]
                        + LinkConfig.getHashCode(utc), getActivity());
                Log.d(TAG + "category", params[0] + LinkConfig.getHashCode(utc));
                return dUtil.downloadStringContent();
            } else
                return utc;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                Log.d(TAG, "internet not available");
                Intent intent = new Intent(getActivity(), NoInternetActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Log.d(TAG, "server_error");
                Intent i = new Intent(getActivity(), UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        getString(R.string.server_unreachable_message));
                i.putExtra("username", "");
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                getActivity().finish();

            } else {
                MovieCategoryParser parser = new MovieCategoryParser(result);
                parser.parse();

                ArrayList<MovieCategory> movieCategories = parser
                        .getMovieCategories();

                new MoviesContentLoader(movieCategories)
                        .execute(moviesContentLinkString);

            }

        }

        private class MoviesContentLoader extends AsyncTask<String, Void, String> {

            private ArrayList<MovieCategory> movieCategories;

            public MoviesContentLoader(ArrayList<MovieCategory> movieCategories) {
                this.movieCategories = movieCategories;
            }

            @Override
            protected String doInBackground(String... params) {

                DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, getActivity());
                String utc = getUtc.downloadStringContent();
                if (!utc.equals(DownloadUtil.NotOnline)
                        && !utc.equals(DownloadUtil.ServerUnrechable)) {

                    DownloadUtil dUtil = new DownloadUtil(params[0]
                            + LinkConfig.getHashCode(utc), getActivity());
                    Log.d(TAG + "content", params[0] + LinkConfig.getHashCode(utc));
                    return dUtil.downloadStringContent();
                } else
                    return utc;
            }


            @Override
            protected void onPostExecute(String result) {
                if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                    Log.d(TAG, "internet not available");
                    Intent intent = new Intent(getActivity(), NoInternetActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                    // server not rechable
                } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                    Log.d(TAG, "server_error");
                    Intent i = new Intent(getActivity(), UnauthorizedAccess.class);
                    i.putExtra("error_code", "");
                    i.putExtra("error_message",
                            getString(R.string.server_unreachable_message));
                    i.putExtra("username", "");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                } else {
                    try {
                        progressBar.setVisibility(View.GONE);
                        MovieParser parser = new MovieParser(result, getActivity());
                        parser.setMovieCategories(movieCategories);

                        parser.parse();

                        HashMap<String, ArrayList<Movie>> genreAndContent = parser
                                .getContent();
                        ArrayList<String> genreHavingContent = parser
                                .getGenreHavingContent();
                        ArrayList<String> genreNotHavingContent = parser
                                .getGenreNotHavingContent();
                        Log.d("genre", movieCategories + "asjga");
                        Log.d("Genreandmoveis", genreAndContent + "");


                        MoviesListAdapter adapter = new MoviesListAdapter(getActivity(), R.layout.movie_horizontal_list, movieCategories, genreAndContent);
                        adapter.notifyDataSetChanged();
                        gv.setAdapter(adapter);
                        for (int i = 0; i < genreHavingContent.size(); i++) {

                            String genreTitle = genreHavingContent.get(i);
                            ArrayList<Movie> moviesOfGenre = genreAndContent
                                    .get(genreTitle);

                        }

                    } catch (Exception je) {
                        je.printStackTrace();

                    }
               /* if (loading.isShowing())
                    loading.dismiss();*/

                }
            }
        }

    }
}





