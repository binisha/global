package com.newitventure.mobile.GlobalHimalayan;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.newitventure.mobile.GlobalHimalayan.R;

public class SignUpWebsite extends ActionBarActivity {
    Toolbar mtoolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_website);
        mtoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mtoolbar);

        mtoolbar.setTitle("Register");

        if (NetworkUtil.isInternetAvailable(SignUpWebsite.this)) {

            WebView myWebView = (WebView) findViewById(R.id.webview);
            myWebView.loadUrl("http://internettv.globalhtv.com/user-signup");

        }
    }
}
