package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;


import com.newitventure.mobile.GlobalHimalayan.Tabs.TvShowsTab;

import java.util.ArrayList;

/**
 * Created by NITV on 8/15/2015.
 */
public class ViewPagerAdapterTvShows extends FragmentPagerAdapter {


    ArrayList<VodCategoryParent> tvShowCategoryTabTitles; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    public ViewPagerAdapterTvShows(FragmentManager fm, ArrayList<VodCategoryParent> tvShowCategoryTabTitles, int mNumbOfTabsumb) {
        super(fm);

        this.tvShowCategoryTabTitles = tvShowCategoryTabTitles;
        this.NumbOfTabs = mNumbOfTabsumb;


    }


    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {


        Fragment fragment = new TvShowsTab();
        Bundle args = new Bundle();
        args.putInt("page_position", position);
        args.putInt("catId" ,tvShowCategoryTabTitles.get(position).getParentId());
        fragment.setArguments(args);
        Log.d("page_position", position + "");
        return fragment;
        //return this.Titles.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d(tvShowCategoryTabTitles.get(position).getCategoryName(),"title name");
        return tvShowCategoryTabTitles.get(position).getCategoryName();
    }


    @Override
    public int getCount() {
        return NumbOfTabs;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }
}


