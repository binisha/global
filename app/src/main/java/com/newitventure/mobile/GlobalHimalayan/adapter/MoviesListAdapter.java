package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.Movie;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategory;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;

public class MoviesListAdapter extends ArrayAdapter<String> {

    private final static String TAG = "LiveChannelListAdapter";


    private Context context;
    private int rowResourceId;
    private ArrayList<MovieCategory> genreList;
    private HashMap<String, ArrayList<Movie>> movieContent;

    //private MainActivity mResponder;

    public MoviesListAdapter(Context context, int textViewResourceId,
                             ArrayList<MovieCategory> genreList, HashMap<String, ArrayList<Movie>> objects) {
        super(context, textViewResourceId);

        this.context = context;
        this.rowResourceId = textViewResourceId;
        this.genreList = genreList;
        this.movieContent = objects;


    }

    @Override
    public int getCount() {

        return genreList.size();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(rowResourceId, null);

            holder = new ViewHolder();

            holder.categoryName = (TextView) convertView.findViewById(R.id.category_name);
            holder.hListView = (TwoWayView) convertView.findViewById(R.id.movie_horizontal_listview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MovieCategory currentGenre = genreList.get(position);
        String genreName = currentGenre.getCategoryName();

        final ArrayList<Movie> categoryMovieList = movieContent.get(currentGenre.getCategoryName());
        holder.categoryName.setText(genreName);

        MoviesHorizontalListAdapter adapter = new MoviesHorizontalListAdapter(context, R.layout.movie_item, categoryMovieList);
        holder.hListView.setAdapter(adapter);
        holder.hListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                final Movie movie = categoryMovieList.get(position);

                LinkConfig.loadMovieLink(context, movie);

            }


        });
        return convertView;
    }

    private class ViewHolder {

        TextView categoryName;
        TwoWayView hListView;

    }

}
