package com.newitventure.mobile.GlobalHimalayan;

/**
 * Created by Dell on 19-Aug-15.
 */

import android.app.AlertDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.facebook.FacebookRequestError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.adapter.ViewPagerAdapter;
import com.newitventure.mobile.GlobalHimalayan.entity.Channel;
import com.newitventure.mobile.GlobalHimalayan.parser.AllParser;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.Mcrypt;
import com.newitventure.mobile.GlobalHimalayan.util.LiveTvVideoControllerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class Player extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, LiveTvVideoControllerView.MediaPlayerControl {

    private Channel playingChannel, channelfrompriority = new Channel();
    private String data, data1 = null;
    private String categoryName, serverType, deryptedUrl;
    private VideoView videoView;
    //private CustomMediaController controller;
    public static LiveTvVideoControllerView controller;
    AlertDialog alertDialog;
    private HashMap<String, ArrayList<Channel>> category_channel;

    private ArrayList<String> categoryNames = AllParser.categorynames;
    private ArrayList<Channel> channellist;
    private ArrayList<FacebookRequestError.Category> categoryNameslist;
    private int currentCategoryIndex;
    private String currentCategory;
    Toolbar toolbar;
    View viewline, viewSecond;
    ViewPager pager;
    ViewPagerAdapter adapter;
    CharSequence Titles[];
    int currentSelectedCategoryIndex;
    int Numboftabs;
    SlidingTabLayout tabs;
    private RelativeLayout mainContainer;
    private View decorView;
    AdView mAdView;
    LinearLayout layout;
    CustomDialogManager loadingDialog;
    ImageButton playButton;
    ProgressBar tvProgressBar;
    private FrameLayout videoContainer;
    private SurfaceView videoSurface;
    private RelativeLayout errorLayout;
    private ImageView retry;
    private TextView errorMsg, serverUnreachableError;
    public static MediaPlayer player;
    private boolean mFullScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        decorView = getWindow().getDecorView();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Numboftabs = categoryNames.size();

        Titles = categoryNames.toArray(new CharSequence[categoryNames.size()]);
        pager = (ViewPager) findViewById(R.id.viewpager);

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), this, Titles, Numboftabs);
        pager.setAdapter(adapter);

        tabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        tabs.setDistributeEvenly(true);
        tabs.setBackgroundColor(getResources().getColor(R.color.primary));
        tabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
        tabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));
        tabs.setViewPager(pager);

        videoContainer = (FrameLayout) findViewById(R.id.video_surface_container);
        videoSurface = (SurfaceView) findViewById(R.id.video_surface);
        playButton = (ImageButton) findViewById(R.id.play_button);

        tvProgressBar = (ProgressBar) findViewById(R.id.progress_bar_live_tv);

        errorLayout = (RelativeLayout) findViewById(R.id.network_error_layout);
        serverUnreachableError = (TextView) findViewById(R.id.server_unreachable_error_msg);
        errorMsg = (TextView) findViewById(R.id.network_error_msg);
        retry = (ImageView) findViewById(R.id.network_retry);

        mAdView = (AdView) findViewById(R.id.gad_bottom);

        getCurrentOrientation();
        loadChannelData();

        SurfaceHolder videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);

        player = new MediaPlayer();
        controller = new LiveTvVideoControllerView(Player.this);


        if (serverType.equals("1")) {

            Mcrypt mCrypt = new Mcrypt();

            try {

                deryptedUrl = new String(mCrypt.decrypt(data));
                data1 = deryptedUrl.trim();
                Log.d("the data1 isss", data1);

            } catch (Exception e1) {

                e1.printStackTrace();

            }

        } else {

            data1 = data;

        }

        Log.d("data string", data);

        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playButton.setVisibility(View.GONE);
                tvProgressBar.setVisibility(View.VISIBLE);
                loadStreamVideo(data1);

            }

        });

        retry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                errorMsg.setVisibility(View.GONE);
                retry.setVisibility(View.GONE);
                tvProgressBar.setVisibility(View.VISIBLE);

                Handler mHand = new Handler();
                mHand.postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        loadStreamVideo(data1);

                    }

                }, 1000);

            }

        });

        videoSurface.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (controller.isShowing()) {

                    controller.hide();

                } else {

                    controller.show();

                }

                return false;

            }

        });

    }

    private void getCurrentOrientation() {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            setFullscreenLayout();

        } else {

            setPortraitLayout();

        }

    }

    public void loadChannelData() {

        try {

            playingChannel = AllParser.getChannelFromId(getIntent().getExtras()
                    .getInt("currentChannelId"));
            data = getIntent().getExtras().getString("url");
            Log.d("the link in player isss", data);
            serverType = getIntent().getExtras().getString("serverType");
            categoryName = getIntent().getExtras().getString("categoryName");


            category_channel = AllParser.category_channel;
            channellist = category_channel.get(categoryName);
            getSupportActionBar().setTitle(playingChannel.getChannelName());

            pager.setCurrentItem(categoryNames.indexOf(categoryName));

            if (mAdView != null) {

                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
                mAdView.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void loadStreamVideo(String link) {

        try {

            player.setOnPreparedListener(this);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(this, Uri.parse(link));
            player.setOnErrorListener(Player.this);

            try {

                player.prepareAsync();

            } catch (Exception e) {

                e.printStackTrace();

            }

        } catch (IllegalArgumentException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

            serverUnreachableError.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        player.setDisplay(holder);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    /**
     * Prepares the media player to play the video
     */
    @Override
    public void onPrepared(MediaPlayer mp) {

        controller.setMediaPlayer(this);
        controller.setAnchorView((FrameLayout) findViewById(R.id.video_surface_container));
        player.start();
        tvProgressBar.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);

    }


    /**
     * Catches the all the errors related with the media player
     */
    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

        try {

            player.reset();
            controller.hide();
            tvProgressBar.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            errorMsg.setVisibility(View.VISIBLE);
            retry.setVisibility(View.VISIBLE);


        } catch (Exception e) {

            e.printStackTrace();

        }

        return false;

    }

    @Override
    public void start() {

        player.start();

    }

    @Override
    public void pause() {

        player.pause();

    }

    @Override
    public int getDuration() {

        return player.getDuration();

    }

    @Override
    public int getCurrentPosition() {

        return player.getCurrentPosition();

    }

    @Override
    public void seekTo(int pos) {

        player.seekTo(pos);

    }

    @Override
    public boolean isPlaying() {

        return player.isPlaying();

    }

    @Override
    public int getBufferPercentage() {

        return 0;

    }

    @Override
    public boolean canPause() {

        return true;

    }

    @Override
    public boolean canSeekBackward() {

        return true;

    }

    @Override
    public boolean canSeekForward() {

        return true;

    }

    @Override
    public boolean isFullScreen() {

        return mFullScreen;

    }


    /**
     * Sets the video player to the Landscape mode or portrait mode on clicking the toogle button
     */
    @Override
    public void toggleFullScreen() {

        if (isFullScreen()) {

            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        } else {

            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            setFullscreenLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            setPortraitLayout();

        }

    }


    /**
     * Sets the layout when the video player is set to the full screen mode
     */
    private void setFullscreenLayout() {

        this.getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //adLinLay.setVisibility(View.GONE);

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoContainer.getLayoutParams();

        //params.addRule( RelativeLayout.ALIGN_PARENT_TOP & RelativeLayout.ALIGN_PARENT_LEFT & RelativeLayout.ALIGN_PARENT_RIGHT & RelativeLayout.ALIGN_PARENT_BOTTOM );

        params.height = metrics.heightPixels;
        params.width = metrics.widthPixels;// + getTitlebarHeight();
        params.rightMargin = 0;
        params.bottomMargin = 0;
//        params.setMargins(0, (getTitlebarHeight() * metrics.heightPixels / metrics.widthPixels / 2), 0, 0);
        params.setMargins(0, 0, 0, 0);
        videoContainer.setLayoutParams(params);
        mFullScreen = true;

    }


    /**
     * Sets the layout when the video player is set to the portrait mode
     */
    private void setPortraitLayout() {

        this.getSupportActionBar().show();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //adLinLay.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;

        //The size of the diagonal in inches is equal to the square root of the height in inches squared plus the width in inches squared.
        double diagonalInches = Math.sqrt(
                (widthInches * widthInches)
                        + (heightInches * heightInches));

        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoContainer.getLayoutParams();

        if (diagonalInches >= 10) {

            params.width = metrics.widthPixels;
            params.height = (int) (440 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        } else if (diagonalInches >= 7) {

            params.width = metrics.widthPixels;
            params.height = (int) (540 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        } else {

            params.width = metrics.widthPixels;
            params.height = (int) (240 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        }

        videoContainer.setLayoutParams(params);
        mFullScreen = false;

    }

    @Override
    public void onResume() {

        super.onResume();

        playButton.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        errorMsg.setVisibility(View.GONE);
        retry.setVisibility(View.GONE);

    }

    @Override
    public void onPause() {

        super.onPause();

        if (player.isPlaying()) {

            player.stop();
            player.reset();

        }

    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        controller.hide();

        try {

            if (player.isPlaying()) {

                player.stop();
                player.release();

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

}

