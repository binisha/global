package com.newitventure.mobile.GlobalHimalayan.entity;

public class MovieCategory {
	
	public static final String MOVIE_CATEGORY_NAME = "category_name";
	public static final String MOVIE_CATEGORY_ID = "category_id";
	
	private String categoryName;
	private int categoryId;
	
	public MovieCategory(String categoryName, int categoryId) {
		this.categoryName = categoryName;
		this.categoryId = categoryId;
	}
	
	public String getCategoryName() {
		return categoryName;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append( "MovieCategory[" )
		.append( "\n\tCategory Name " ).append( categoryName )
		.append( "\n\tCategory ID " ).append( categoryId )
		.append( "\n]");
		
		return sb.toString();
		
	}
}
