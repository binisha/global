package com.newitventure.mobile.GlobalHimalayan.Tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.adapter.ChannelListAdapter;
import com.newitventure.mobile.GlobalHimalayan.entity.Channel;
import com.newitventure.mobile.GlobalHimalayan.fragment.MultipleUsedStuffs;
import com.newitventure.mobile.GlobalHimalayan.parser.AllParser;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dell on 14-Aug-15.
 */
public class LiveTab extends Fragment {

    private ArrayList<Channel> channelList;
    private ArrayList<String> categoryList;
    HashMap<String, ArrayList<Channel>> category_channel = AllParser.category_channel;
    private String currentCategory;
    private Channel currentChannel;
    private ArrayList<String> categoryNames;
    private String channelLinkUrl, ipAddress;
    private int currentSelectedCategoryIndex, currentClickedChannelPosition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_1, container, false);
        GridView gv = (GridView) v.findViewById(R.id.grid);


        Bundle bundle = this.getArguments();
        int position = bundle.getInt("page_position");
        channelList = new ArrayList<Channel>();
        categoryList = AllParser.categorynames;

        int index = currentSelectedCategoryIndex + position;
        if (index > categoryList.size() - 1) {
            int temp = index - categoryList.size() + 1;

            temp -= 1; // index starts at 0

            index = temp;
        }

        if (index < 0)
            index = 0;

        currentCategory = categoryList.get(index);
        channelList = category_channel.get(currentCategory);

        ChannelListAdapter adapter = new ChannelListAdapter(getActivity(),
                R.layout.tab_row, channelList);
        gv.setAdapter(adapter);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentClickedChannelPosition = position;
                currentChannel = channelList.get(position);

                MultipleUsedStuffs.checkToLoadChannelLink(getActivity(),
                        currentChannel, currentCategory, false);

            }
        });
        return v;
    }

}
