package com.newitventure.mobile.GlobalHimalayan.fragment;


import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    private TextView userEmailView,signUpDate,telephoneView, addressView, cityView,stateView,postalView, countryView,activationStatus,expiryStatus;
    private TextView userDisplayNameView;
    private TextView userNameView;
    public static String userid;
    private SharedPreferences app_preferences;
    TextView textView;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_account, container, false);
        userEmailView = (TextView) v.findViewById(R.id.emailView);
        userDisplayNameView =  (TextView) v.findViewById(R.id.displayNameView);
        activationStatus = (TextView) v.findViewById(R.id.activationStatus);
        signUpDate = (TextView) v.findViewById(R.id.signUpDateStatus);
        telephoneView =  (TextView) v.findViewById(R.id.telephoneStatus);
        addressView = (TextView) v.findViewById(R.id.addressStatus);
        textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
        textView.setText("Account");
        stateView = (TextView) v.findViewById(R.id.stateStatus);
        postalView = (TextView) v.findViewById(R.id.postalCodeStatus);
        cityView = (TextView) v.findViewById(R.id.cityStatus);
        countryView =  (TextView) v.findViewById(R.id.countryStatus);
        expiryStatus = (TextView) v.findViewById(R.id.expiryDateStatus);
        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");
        Log.d("url", LinkConfig.getString(getActivity(), LinkConfig.USER_URL) + "?userID=" + userid );
        new UserAccountInfoLoader().execute( LinkConfig.getString(getActivity(), LinkConfig.USER_URL) + "?userID=" + userid );
        return v;

    }

    private class UserAccountInfoLoader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            Log.d("Account", params[0]);
            DownloadUtil dUtil = new DownloadUtil( params[0], getActivity() );

            return dUtil.downloadStringContent();

        }

        @Override
        protected void onPostExecute(String result) {

            if( result != null && ! "".equals( result ) ) {
                try {

                    JSONObject root = new JSONObject( result );

                    String userEmail = root.getString( "user_email" );
                    Log.d("user email is",userEmail);
                    String displayName = root.getString( "display_name" );
                    String expiryDate = root.getString( "expiry_date" );
                    String signupDate = root.getString( "signup_date" );
                    String activtionStatus = root.getString( "activation_status" );
                    String telephone = root.getString( "telephone" );
                    String address = root.getString( "address" );
                    String city = root.getString( "city" );
                    String state = root.getString( "state" );
                    String postal = root.getString( "postal_code" );
                    String country = root.getString( "country" );

                    userEmailView.setText(userEmail);
                    userDisplayNameView.setText(displayName);
                    expiryStatus.setText(expiryDate);
                    signUpDate.setText(signupDate);
                    System.out.println(activtionStatus);
                    addressView.setText(address);
                    if(activtionStatus.equals("1")){

                        activationStatus.setText("Active");

                    }else {

                        activationStatus.setText("Disabled");

                    }

                    telephoneView.setText(telephone);
                    cityView.setText(city);
                    stateView.setText(state);
                    postalView.setText(postal);
                    countryView.setText(country);


                }catch( JSONException e ) {

                    e.printStackTrace();

                }catch( Exception e ) {

                    e.printStackTrace();

                }

            }

        }

    }

}
