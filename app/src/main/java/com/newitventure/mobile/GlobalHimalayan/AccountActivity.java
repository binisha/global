package com.newitventure.mobile.GlobalHimalayan;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountActivity extends ActionBarActivity {

    private TextView userEmailView;
    private TextView userDisplayNameView;
    private TextView userNameView;
    private TextView boxIdView;
    public static String userid;

    private SharedPreferences app_preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        userEmailView = (TextView) findViewById( R.id.emailAddressView );
        userDisplayNameView =  (TextView) findViewById( R.id.displayNameView );
        userNameView = (TextView) findViewById( R.id.userNameView );
        boxIdView =  (TextView) findViewById( R.id.boxIdView );

        app_preferences = PreferenceManager.getDefaultSharedPreferences(AccountActivity.this);
        userid = app_preferences.getString("userid", "");

        new UserAccountInfoLoader().execute( LinkConfig.getString(AccountActivity.this, LinkConfig.USER_URL) + "?userID=" + userid );
    }

    private class UserAccountInfoLoader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.d("Account", params[0]);
            DownloadUtil dUtil = new DownloadUtil( params[0], getApplicationContext() );

            return dUtil.downloadStringContent();
        }

        @Override
        protected void onPostExecute(String result) {
            if( result != null && ! "".equals( result ) ) {
                try {

                    JSONObject root = new JSONObject( result );

                    String userEmail = root.getString( "user_email" );
                    Log.d("user email is",userEmail);
                    String displayName = root.getString( "display_name" );
                    String boxId = root.getString( "user_boxID" );
                    String userName = root.getString( "username" );

                    userEmailView.setText(userEmail);
                    userDisplayNameView.setText(displayName);
                    userNameView.setText(userName);
                    boxIdView.setText(boxId);

                }catch( JSONException e ) {
                    e.printStackTrace();
                }catch( Exception e ) {
                    e.printStackTrace();
                }
            }
        }
    }

}
