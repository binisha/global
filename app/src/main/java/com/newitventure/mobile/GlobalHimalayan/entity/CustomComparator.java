package com.newitventure.mobile.GlobalHimalayan.entity;

import java.util.Comparator;

//import com.newitventure.wodyoutubeplus.YoutubePlus.CustomComparator;

/**following class to implement comparing dates.**/
public class CustomComparator implements Comparator<CustomComparator> {
	private String url;
	private String thumb;
	private String title;
	private String date;
	private String position;
	private String playlistId;
	private String youtube;
	private String duration;
	
 
	public CustomComparator(String url, String thumb, String title, String date, String position,
							String playlistId, String youtube, String duration) {
		super();
		this.url = url;
		this.thumb = thumb;
		this.title = title;
		this.date = date;
		this.position = position;
		this.playlistId = playlistId;
		this.youtube = youtube;
		this.duration = duration;
	}
 
	public CustomComparator() {
		// TODO Auto-generated constructor stub
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPlaylistId() {
		return playlistId;
	}
	public void setPlaylisId(String playlistId) {
		this.playlistId = playlistId;
	}
	public String getYoutube() {
		return youtube;
	}
	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duraion) {
		this.duration = duraion;		
	}		 	 
	
	 	@Override
	    public int compare(CustomComparator o1, CustomComparator o2) {
	        return o2.getDate().compareTo(o1.getDate());
	    }
   	
}
