package com.newitventure.mobile.GlobalHimalayan.fragment;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.MainActivity;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.Tabs.LiveTab;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.parser.AllParser;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import java.util.ArrayList;

/**
 * Created by Dell on 14-Aug-15.
 */
public class LiveFragment extends Fragment {

    ViewPager viewPager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[];
    int NumberOfTabs;
    private ArrayList<String> categoryList;
    SharedPreferences app_preferences;
    public static String userid, sessionid, userGroup;
    RelativeLayout relativeLayout;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_live, container, false);

        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");
        Log.d("the user id in fag is", userid);
        sessionid = app_preferences.getString("sessionid", "");
        userGroup = app_preferences.getString("user_group", "");
        Log.d("the sessionid in fag is", sessionid);

        tabs = (SlidingTabLayout) v.findViewById(R.id.sliding_tabs_live_channels);
        tabs.setDistributeEvenly(true);
        tabs.setBackgroundColor(getResources().getColor(R.color.primary));
        tabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
        tabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));

        textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
        textView.setText("Live Channels");

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);

        relativeLayout = (RelativeLayout) v.findViewById(R.id.relative_layout);

        String categoryUrl = LinkConfig.getString(getActivity(),
                LinkConfig.CATEGORY_URL);

        relativeLayout.setVisibility(View.VISIBLE);
        new CategoryChannelsLoader().execute(categoryUrl
                + "?groudID=" + userGroup + "&userId="
                + userid);

        return v;
    }

    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        ArrayList<String> Titles;
        int numberofTbs;

        public ViewPagerAdapter(FragmentManager fm, ArrayList<String> titles, int numberOfTabs) {
            super(fm);
            this.Titles = titles;
            this.numberofTbs = numberOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new LiveTab();
            Bundle args = new Bundle();
            args.putInt("page_position", position);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles.get(position);
        }

        @Override
        public int getCount() {
            return numberofTbs;
        }

    }

    private class CategoryChannelsLoader extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            Log.d("test data", params[0]);

            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC,
                    getActivity());
            String utc = getUtc.downloadStringContent();

            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {
                DownloadUtil dUtil = new DownloadUtil(params[0] + "&"
                        + LinkConfig.getHashCode(utc), getActivity());

                Log.d("MOVIE PARENT", params[0] + "&"
                        + LinkConfig.getHashCode(utc));

                return dUtil.downloadStringContent();
            } else
                return utc;

        }

        @Override
        protected void onPostExecute(String result) {

            try {

                if (result != null) {

                    relativeLayout.setVisibility(View.GONE);
                    AllParser parser = new AllParser(getActivity(), result);
                    parser.parse();
                    categoryList = AllParser.categorynames;

                    Titles = categoryList.toArray(new CharSequence[categoryList.size()]);

                    NumberOfTabs = categoryList.size();
                    adapter = new ViewPagerAdapter(getChildFragmentManager(), categoryList, NumberOfTabs);
                    viewPager.setAdapter(adapter);
                    tabs.setViewPager(viewPager);

                }

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }

}
