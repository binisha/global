package com.newitventure.mobile.GlobalHimalayan.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.LoginActivity;
import com.newitventure.mobile.GlobalHimalayan.R;


public class UnauthorizedAccess extends Activity {

	private TextView messageView,txt_username;
	private Button retry;
	private String username,macAddress="",error_code="",error_message="";
	private StringBuilder mb = new StringBuilder();
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unauthorized_access);
		messageView = (TextView) findViewById(R.id.unauthorized_message);
		txt_username = (TextView) findViewById(R.id.username);
		retry =(Button)findViewById(R.id.retry);
		retry.requestFocus();

		error_code=getIntent().getStringExtra("error_code");
		error_message = getIntent().getStringExtra("error_message");
		Log.e(error_code + "", error_message + "");
		try{
			username = getIntent().getStringExtra("username");
			txt_username.setText(username);
			txt_username.setVisibility(View.VISIBLE);
			Log.e("username", username);
		}catch(Exception e){
			txt_username.setVisibility(View.GONE);
		}
		
		if(error_code.equals(""))
			mb.append("Box ID: "+ macAddress. toUpperCase())
			.append( "\n\n" )
			.append( error_message );
		
		else
			mb.append("Box ID: "+ macAddress. toUpperCase())
			.append( "\n\n" )
			.append("Error Code: "+ error_code)
			.append("\n")
			.append( error_message );
			
		messageView.setText( mb.toString() );
		retry.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(UnauthorizedAccess.this,LoginActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		});
	}
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
