package com.newitventure.mobile.GlobalHimalayan.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.newitventure.mobile.GlobalHimalayan.entity.Channel;


/**
 * This is the class where all parsing is done
 * categories and the channel is parsed in this class
 */
public class AllParser implements Comparator<Channel> {
    public static ArrayList<String> categorynames;
    private String jsonString;
    public static HashMap<String, ArrayList<Channel>> category_channel = null;
    public static ArrayList<Channel> allchannels = null;
    private ArrayList<Channel> channellist;
    private Context context;

    public AllParser(Context context, String jsonString) {
        this.jsonString = jsonString;
        this.categorynames = new ArrayList<String>();
        category_channel = new HashMap<String, ArrayList<Channel>>();
        allchannels = new ArrayList<Channel>();


    }

    public void parse() {
        try {
            JSONObject root = new JSONObject(jsonString);

            //parsing list of categories
            JSONArray categories = root.getJSONArray("categories");
            Log.d("length of json array of categories",categories.length()+"");
            Log.d("jsonArray of categories", categories.toString());
            for (int i = 0; i < categories.length(); i++) {
                Log.d(i+"", categories.get(i)+"");
                String categoryName = (String) categories.get(i);
                Log.d(i+"", categoryName);
                this.categorynames.add(categoryName);
            }
            //end of parsing categories

            JSONObject items = root.getJSONObject("items");

            //parsing of list of channels in each categories
            Log.d("item ako cha ki chaina", items+"");
            for (int totalCategories = 0; totalCategories < categorynames.size(); totalCategories++) {
                if(categorynames.get(totalCategories).equalsIgnoreCase("all")||categorynames.get(totalCategories).equalsIgnoreCase("favourite"))
                    continue;
                Log.d("parsing now of category out of"+ categories.length(), categorynames.get(totalCategories));
                channellist = new ArrayList<Channel>();
                JSONArray channelsInCategories = items
                        .getJSONArray(this.categorynames.get(totalCategories));

                for (int totalChannels = 0; totalChannels < channelsInCategories
                        .length(); totalChannels++) {

                    JSONObject channel = channelsInCategories
                            .getJSONObject(totalChannels);

                    Channel ch = new Channel();
                    ch.setChannelId(Integer.parseInt(channel
                            .getString("channel_id")));
                    ch.setChannelName(channel.getString("channel_name"));
                    ch.setChannelCountry(channel.getString("channel_country"));
                    ch.setChannelLogoLink(channel.getString("channel_logo"));
                    ch.setChannelDescription(channel.getString("channel_desc"));
                    ch.setChannelPriority(channel.getInt("channel_priority"));
                    ch.setChannelLanguage(channel.getString("channel_language"));
                    try {
                        ch.setFavourite(channel.getInt("favorite"));
                    } catch (Exception e) {
                        ch.setFavourite(0);
                    }

                    channellist.add(ch);
                    //end of parsing channels
                    allchannels.add(ch);

                }

                //Collections.sort(channellist, this);
                Collections.sort(allchannels, this);

                category_channel.put(categorynames.get(totalCategories),
                        channellist);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compare(Channel lhs, Channel rhs) {
        return lhs.getChannelPriority() - rhs.getChannelPriority();
    }

    public static ArrayList<Channel> favoriteCategoryChannels(
    ) {
        ArrayList<Channel> favchannels = new ArrayList<Channel>();
        for (int i = 0; i <AllParser.allchannels.size(); i++) {
            Channel ch = AllParser.allchannels.get(i);
            if (ch.getFavourite() == 1)
                favchannels.add(ch);
            else
                continue;
        }

        return favchannels;

    }
    public static Channel getChannelFromId(int channel_id) {
        for (int i = 0; i < allchannels.size(); i++) {
            if (channel_id == allchannels.get(i).getChannelId()) {
                return allchannels.get(i);
            }
        }
        return null;
    }

}
