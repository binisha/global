package com.newitventure.mobile.GlobalHimalayan.entity;

/**
 * Created by nitv on 12/27/15.
 */
public class YoutubePlaylistObj {

    private String thumbnail;
    private String Title;

    public void setThumbnail(String thumbnail) {

        this.thumbnail = thumbnail;

    }

    public String getThumbnail() {

        return thumbnail;

    }

    public void setTitle(String Title) {

        this.Title = Title;

    }

    public String getTitle() {

        return Title;

    }
}