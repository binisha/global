package com.newitventure.mobile.GlobalHimalayan.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.adapter.ViewPagerAdapterTvShows;
import com.newitventure.mobile.GlobalHimalayan.adapter.VodCategoryParent;
import com.newitventure.mobile.GlobalHimalayan.parser.VodCategoryParentParser;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.CustomViewPager;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TvshowsFragment extends Fragment {
    ArrayList<VodCategoryParent> tvShowCategoryParentList;
    Context context;
    private CustomViewPager pager;
    private SlidingTabLayout slidingTabs;
    private SharedPreferences app_preferences;
    String userid, userGroup, sessionid;
    public RelativeLayout relativeLayout;
    TextView textView;

    public TvshowsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tvshows, container, false);
        pager = (CustomViewPager) v.findViewById(R.id.tvshow_viewpager);
        pager.setPagingEnabled(true); //set false to disable swipe
        slidingTabs = (SlidingTabLayout) v.findViewById(R.id.tvshow_Sliding_Tabs);

        textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
        textView.setText("Tv Shows");

        relativeLayout = (RelativeLayout) v.findViewById(R.id.relative_layout);

        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");
        sessionid = app_preferences.getString("sessionid", "");
        userGroup = app_preferences.getString("user_group", "");
        String youtubeCategoryURL = LinkConfig.getString(getActivity(), LinkConfig.YOUTUBE_CATEGORY) + "?groupId=" + userGroup;
        Log.d("URL", youtubeCategoryURL);
        new TvShowCategoryJsonParser().execute(youtubeCategoryURL);

        return v;
    }


    private class TvShowCategoryJsonParser extends
            AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            relativeLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            DownloadUtil dUtil = new DownloadUtil(params[0], context);

            return dUtil.downloadStringContent();

        }

        protected void onPostExecute(String result) {

            try {

                if (result != null) {
                    final int highestPosition = result.length() - 1;

                    VodCategoryParentParser parser = new VodCategoryParentParser(
                            result, context);
                    parser.parse();

                    tvShowCategoryParentList = parser.getMovieCategoryParentList();
                    Log.d("Highest Position", highestPosition + "");

                    relativeLayout.setVisibility(View.GONE);

                    ViewPagerAdapterTvShows adapter = new ViewPagerAdapterTvShows(getChildFragmentManager(), tvShowCategoryParentList, tvShowCategoryParentList.size());
                    pager.setAdapter(adapter);

                    slidingTabs.setDistributeEvenly(true);
                    slidingTabs.setBackgroundColor(getResources().getColor(R.color.primary));
                    slidingTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
                    slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));
                    slidingTabs.setViewPager(pager);

                }

            } catch (Exception e) {

                e.printStackTrace();
            }

        }

    }

}
