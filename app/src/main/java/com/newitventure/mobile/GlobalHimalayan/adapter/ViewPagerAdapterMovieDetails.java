package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;


import com.newitventure.mobile.GlobalHimalayan.Tabs.MovieTab;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategoryParent;
import com.newitventure.mobile.GlobalHimalayan.fragment.MoviesPlayingFragmet;

import java.util.ArrayList;

/**
 * Created by NITV on 8/12/2015.
 */
public class ViewPagerAdapterMovieDetails extends FragmentStatePagerAdapter {

    ArrayList<MovieCategoryParent> MovieCategoryTabTitles; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapterMovieDetails(FragmentManager fm, ArrayList<MovieCategoryParent> MovieCategoryTabTitles, int mNumbOfTabsumb) {
        super(fm);

        this.MovieCategoryTabTitles = MovieCategoryTabTitles;
        this.NumbOfTabs = mNumbOfTabsumb;


    }



    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            Fragment fragment = new MoviesPlayingFragmet();

            return fragment;

        } else {
            Fragment fragment = new MovieTab();
            Bundle args = new Bundle();
            args.putInt("page_position", position);
            fragment.setArguments(args);
            Log.d("page_position", position + "");
            return fragment;
            //return this.Titles.get(position);
        }
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d(MovieCategoryTabTitles.get(position).getCategoryName(), "title name");
        return MovieCategoryTabTitles.get(position).getCategoryName();
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

}