package com.newitventure.mobile.GlobalHimalayan.util;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.newitventure.mobile.GlobalHimalayan.Player;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.Channel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dell on 19-Aug-15.
 */
public class ChannelLinkLoader extends AsyncTask<String, Void, String> {

    private static final String TAG = "com.newitventure.worldondemand.livetv.ChannelLinkLoader";
    private Context context;
    private Channel channel;
    private String categoryName, tvUrl, serverType;

    private boolean flag_to_close_activity;
    String success;
    private String error_message = "";
    private String error_code = "";

    CustomDialogManager loadingDialog;

    public ChannelLinkLoader(Context context, Channel channel,
                             String categoryName, boolean flag_to_close_activity) {
        this.context = context;
        this.channel = channel;
        this.categoryName = categoryName;
        this.flag_to_close_activity = flag_to_close_activity;

    }

    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        String message = context.getString(R.string.txt_loading);
        loadingDialog = new CustomDialogManager(context, message, CustomDialogManager.LOADING);
        loadingDialog.build();
        loadingDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, context);
        String utc = getUtc.downloadStringContent();

        DownloadUtil dUtil = new DownloadUtil(params[0]
                + LinkConfig.getHashCode(utc), context);
        Log.d(TAG, params[0] + LinkConfig.getHashCode(utc));

        return dUtil.downloadStringContent();

    }

    @Override
    protected void onPostExecute(String result) {

        JSONObject root = null;
        try {
            root = new JSONObject(result);
            tvUrl = root.getString("link");
            Log.d("link of channel isss", tvUrl);
            serverType = root.getString("server_type");
            Log.d("servertype  of channel isss", serverType);
            Log.d("channel link", tvUrl);
            Bundle basket = new Bundle();
            basket.putString("url", tvUrl);
            basket.putString("serverType", serverType);
            basket.putString("categoryName", categoryName);;

            loadingDialog.dismiss();

            Intent i = new Intent(context, Player.class);
            i.putExtras(basket);
            i.putExtra("currentChannelId", channel.getChannelId());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
