package com.newitventure.mobile.GlobalHimalayan;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.adapter.GridAdapter;
import com.newitventure.mobile.GlobalHimalayan.entity.Item;

import java.util.ArrayList;


public class Home extends ActionBarActivity {

  ArrayList<Item> itemlist;
    GridView gridView;
   Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        gridView = (GridView) findViewById(R.id.gridview);
         loginButton = (Button) findViewById(R.id.login_btn);
        itemlist = new ArrayList<Item>();

        itemlist.add(new Item("Live TV",R.drawable.live_tv));
        itemlist.add(new Item("Movie", R.drawable.movies));
        itemlist.add(new Item("YoutubePlus", R.drawable.youtube_plus));
        itemlist.add(new Item("TV Show", R.drawable.tv_shows));
        itemlist.add(new Item("Account", R.drawable.my_account));
        itemlist.add(new Item("SETTINGS", R.drawable.setting_102x104));


        GridAdapter adapter = new GridAdapter(this,itemlist,R.layout.gridview_layout);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this,LoginActivity.class);
                startActivity(intent);

            }
        });

    }




}
