package com.newitventure.mobile.GlobalHimalayan;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.newitventure.mobile.GlobalHimalayan.R;

public class TermsofService extends ActionBarActivity {
    Toolbar mtoolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termsof_service);
        mtoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mtoolbar.setTitle("Terms and Service");
        if (NetworkUtil.isInternetAvailable(TermsofService.this)) {

            WebView myWebView = (WebView) findViewById(R.id.webview);
            myWebView.loadUrl("http://internationaltv.sabhkuchh.com/page-6");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
