package com.newitventure.mobile.GlobalHimalayan;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.R;

/**
 * Created by Dell on 02-Sep-15.
 */
public class DoubleLoginActivity extends ActionBarActivity{

    Button relogin;
    TextView exit_doublelogin;
    SharedPreferences app_preferences;
    Toolbar mtoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doublelogin);
        exit_doublelogin= (TextView)findViewById(R.id.exit_doublelogin_text);
        exit_doublelogin.setText(getString(R.string.app_name) + " " + getString(R.string.exit_doublelogin));
        relogin =((Button)findViewById(R.id.relogin));
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        mtoolbar.setTitle("Mult-Login Detected");
        relogin.requestFocus();
        app_preferences = PreferenceManager.getDefaultSharedPreferences(DoubleLoginActivity.this);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.clear();
        editor.apply();
        relogin.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        app_preferences = PreferenceManager.getDefaultSharedPreferences(DoubleLoginActivity.this);
                        SharedPreferences.Editor editor = app_preferences.edit();
                        editor.clear();
                        editor.apply();

                        System.exit(0);

                    }
                });
    }

}
