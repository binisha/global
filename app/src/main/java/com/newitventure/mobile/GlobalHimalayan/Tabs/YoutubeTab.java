package com.newitventure.mobile.GlobalHimalayan.Tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.YoutubePlayActivity;
import com.newitventure.mobile.GlobalHimalayan.adapter.YoutubePlaylist;
import com.newitventure.mobile.GlobalHimalayan.entity.YoutubePlaylistObj;
import com.newitventure.mobile.GlobalHimalayan.parser.JSONParserYoutube;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.google.android.gms.internal.zzip.runOnUiThread;


/**
 * Created by NITV on 8/6/2015.
 */
public class YoutubeTab extends Fragment {


    private SharedPreferences app_preferences;
    View v;
   // private GridView lv;
    private ListView lv;
    int position;
    String userid;


    private static final String TAG_CATEGORY = "items";
    private static final String TAG_CATEGORY_ID = "category_id";
    private static final String TAG_CATEGORY_TITLE = "category_title";
    private static final String TAG_CATEGORY_THUMBNAIL = "thumbnails";


    //private static String GOOGLE_API_KEY = "AIzaSyBYkoGH-gYcpICI1n1JNToRmC_7DUYo8Rs";
    private static String GOOGLE_API_KEY = "AIzaSyBo6uINWMVwhPFzCXFhML5H7PM5Ve-DvTk";//this one is the browser key


    JSONArray categories;
    ArrayList<HashMap<String, String>> categoryList;
    ArrayList<YoutubePlaylistObj> youtubePlaylistArray;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle bundle = this.getArguments();
        position  = bundle.getInt("page_position");
        String channnelID = bundle.getString("youtube_channelId");

        v = inflater.inflate(R.layout.youtube_tab, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);


        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");

        lv = (ListView) v.findViewById(R.id.youTubeList);

        new AsyncTaskToFetchJson(channnelID).execute();

        return v;

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    public class AsyncTaskToFetchJson extends AsyncTask<String, Void, String> {

        String channelId;

        public AsyncTaskToFetchJson(String channelId) {

            this.channelId = channelId;
        }

        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);

        }


        protected void onPostExecute(String result) {

            progressBar.setVisibility(View.GONE);
            lv.setSelection(0);

        }

        @Override
        protected String doInBackground(String... arg0) {

            categoryFeed(channelId);

            return null;

        }
    }

    public void categoryFeed(String catIdVOD) {
        categoryList = new ArrayList<HashMap<String, String>>();
        youtubePlaylistArray = new ArrayList<YoutubePlaylistObj>();
        try {
            JSONParserYoutube categoryParser = new JSONParserYoutube();
            String jsonUrl = "https://www.googleapis.com/youtube/v3/playlists?key="
                    + GOOGLE_API_KEY
                    + "&channelId="
                    + catIdVOD
                    + "&part="
                    + "snippet,contentDetails&maxResults=15&fields=items(etag,id,snippet/title,snippet/description,snippet/thumbnails/default/url),nextPageToken&pageToken=";
            JSONObject category = categoryParser.getJsonFromYoutube(jsonUrl);
            categories = category.getJSONArray(TAG_CATEGORY);
            String nextPage = "";

            do {
                for (int i = 0; i < categories.length(); i++) {
                    JSONObject jO = categories.getJSONObject(i);
                    // String catId=jO.getString(TAG_CATEGORY_ID);
                    String catTitle = jO.getJSONObject("snippet").getString(
                            "title");
                    String catThumbnail =   jO.getJSONObject("snippet")
                            .getJSONObject("thumbnails")
                            .getJSONObject("default")
                            .getString("url");

                    YoutubePlaylistObj obj = new YoutubePlaylistObj();
                    obj.setTitle(catTitle);
                    obj.setThumbnail(catThumbnail);

                    youtubePlaylistArray.add(obj);
                   // Log.d("andy", "object=" +youtubePlaylistObj);

                    String playListNo = jO.getString("id");
                    // String youtubeNo = jO.getString("youtube");
                    HashMap<String, String> map = new HashMap<String, String>();
                    // map.put(TAG_CATEGORY_ID, catId);
                    map.put(TAG_CATEGORY_TITLE, catTitle);
                    map.put("url", playListNo);
                    map.put(TAG_CATEGORY_THUMBNAIL, catThumbnail);
                    map.put(TAG_CATEGORY_THUMBNAIL,
                            jO.getJSONObject("snippet")
                                    .getJSONObject("thumbnails")
                                    .getJSONObject("default")
                                    .getString("url"));

                    // map.put("youtube", youtubeNo);
                    categoryList.add(map);
                }
                if (category.has("nextPageToken")) {
                    nextPage = category.getString("nextPageToken");
                    String jsonUrl2 = new StringBuilder().append(jsonUrl)
                            .append(nextPage).toString();
                    category = categoryParser.getJsonFromYoutube(jsonUrl2);
                    if (category.has(TAG_CATEGORY)) {
                        categories = category.getJSONArray(TAG_CATEGORY);
                    } else {
                        nextPage = "";
                    }
                } else {
                    nextPage = "";
                }

            } while (nextPage != "");

            final YoutubePlaylist adapter = new YoutubePlaylist(getActivity(), youtubePlaylistArray);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    // stuff that updates ui
                    lv.setAdapter(adapter);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent i = new Intent(getActivity(), YoutubePlayActivity.class);
                            i.putExtra("playlist_id", categoryList.get(position).get("url"));
                            i.putExtra("Category_title", categoryList.get(position).get(TAG_CATEGORY_TITLE));
                            i.putExtra("category_size", categoryList.size());
                            startActivity(i);

                        }
                    });
                }
            });

        } catch (JSONException e) {
            Log.e("YOUTUBE", "JSON Exception:");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        getData();
    }


    public ArrayList<YoutubePlaylistObj> getData(){

        try{

            if (youtubePlaylistArray.size() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(getActivity(),
                                "Could not find  the content for this category",
                                Toast.LENGTH_LONG).show();
                    }
                });

            }
            return youtubePlaylistArray;

        }catch (Exception e){

            e.printStackTrace();
        }

        return null;
    }
}
