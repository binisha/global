package com.newitventure.mobile.GlobalHimalayan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.Channel;
import com.newitventure.mobile.GlobalHimalayan.entity.Movie;
import com.newitventure.mobile.GlobalHimalayan.fragment.LiveFragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Dell on 15-Aug-15.
 */
public class LinkConfig {
    public static final String BASE_URL = "http://internationaltv.sabhkuchh.com/mobile/";
    public static final String CHECK_IF_SERVER_RECHABLE = "internationaltv.sabhkuchh.com";

    //    livetv
    public static final int CATEGORY_URL = R.string.category_url;
    public static final String GET_UTC = BASE_URL + "other/" + "get_utc.php";
    public static final int GET_SESSION = R.string.get_session_url;
    public static final int LOGIN_BUTTON_CLICK = R.string.login_btn_click_url;
    public static final int CHANNEL_LINK_URL = R.string.channel_link_url;
    //    movies
    public static final int GETMOVIE_JSON = R.string.getmovie_json;
    public static final int lISTMOVIE_JSON = R.string.listmovie_json;
    public static final int MOVIE_CATEGORY = R.string.movie_category;
    public static final int MOVIE_CATEGORY_BK = R.string.movie_category_bk;
    public static final int MOVIE_PARENT_CATEGORY = R.string.movie_parent_category;

    // account
    public static final int USER_URL = R.string.user_url;

    public static final int CACHE_HOLD_DURATION = 2 * 60 * 60 * 1000;

    /*youtube*/
    public static final int YOUTUBE_CATEGORY = R.string.youtube_category_url;
    public static final int YOUTUBE_PLAYLIST = R.string.youtube_playlist_url;
    //multilogin

    public static final int MULTI_LOGIN = R.string.multilogin_url;
    public static SharedPreferences app_preferences;
    public static String userid;


    public static String getString(Context context, int resId) {

        return BASE_URL + context.getResources().getString(resId);

    }

    public static String getHashCode(String utc) {

        String sessionId = null;
        String userId;
        try {

            sessionId = LiveFragment.sessionid;
            userId = LiveFragment.userid;
            Log.d("session id in linkcongig is iss", "" + sessionId);

            Log.d("user id iss", "ss" + userId);

            if (sessionId != null) {

                String SecretKey = "123456789";
                Log.d("sessionId", sessionId + "sessionId");

                Log.d("utc time", utc + "");
                String stringToMD5 = SecretKey + sessionId
                        + userId + utc;
                String hexString = md5(stringToMD5);

                return "utc=" + utc.trim() + "&hash=" + hexString.toString();

            } else {

                return null;

            }

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        }

    }


    private static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {

                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();

        }

        return "";
    }

    public static void loadChannelLink(final Context context, final Channel channel,
                                       final String currentCategory, final boolean flag_to_end_activity) {

        final String channelLinkUrl = LinkConfig.getString(context,
                LinkConfig.CHANNEL_LINK_URL) + "?";
        UtilsIpMac ipMac = new UtilsIpMac();
        ipMac.getIpAddress(new UtilsIpMac.VolleyCallback() {

            @Override
            public void onSuccess(String result) {

                app_preferences = PreferenceManager.getDefaultSharedPreferences(context);
                userid = app_preferences.getString("userid", "");

                new ChannelLinkLoader(context, channel, currentCategory + "",
                        flag_to_end_activity).execute(channelLinkUrl + "channelID="
                        + channel.getChannelId() + "&ip_address=" + result + "&userId=" + userid + "&");
            }

        }, context);

    }

    public static void loadMovieLink(Context context, Movie movie) {

        int movieId = movie.getMovieId();

        final String media_url = "http://internationaltv.sabhkuchh.com/" + LinkConfig.GETMOVIE_JSON + "?movieId=" + movieId;


        new MovieLinkLoader(context, movie, movieId)
                .execute(media_url);

    }

}
