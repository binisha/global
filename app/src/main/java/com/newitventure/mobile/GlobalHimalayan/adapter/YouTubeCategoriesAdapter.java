package com.newitventure.mobile.GlobalHimalayan.adapter;

public class YouTubeCategoriesAdapter {
	private String parentId;
	private String categoryName;
	private String categoryDescription;
	private String categoryImageLink;
		
	public void setParentId( String string ) {
		this.parentId = string;
	}
	
	public void setCategoryName( String categoryName ) {
		this.categoryName = categoryName;
	}
	
	public void setCategoryDescription( String categoryDescription ) {
		this.categoryDescription = categoryDescription;
	}
	
	public void setCategoryImageLink( String categoryImageLink ) {
		this.categoryImageLink = categoryImageLink;
	}
	
	public String getParentId() {
		return this.parentId;
	}
	
	public String getCategoryName() {
		return this.categoryName;
	}
	
	public String getCategoryDescription() {
		return this.categoryDescription;
	}
	
	public String getCategoryImageLink() {
		return this.categoryImageLink;
	}	

	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append( "MovieCategoryParent[" )
			.append( "\n\tParent ID: ").append( this.parentId )
			.append( "\n\tCategory Name: ").append( this.categoryName )
			.append( "\n\tCategory Description: ").append( this.categoryDescription )
			.append( "\n\tCategory Image: ").append( this.categoryImageLink )			
			.append( "\n]" );
		
		return sb.toString();
			
	}

}
