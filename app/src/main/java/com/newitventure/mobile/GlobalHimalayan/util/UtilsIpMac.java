package com.newitventure.mobile.GlobalHimalayan.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by Dell on 19-Aug-15.
 */
public class UtilsIpMac {

     String ip;

    public String getIpAddress(final VolleyCallback callback, Context context) {

        String url = "http://ip2country.sourceforge.net/ip2c.php?format=JSON";

        RequestQueue queue = Volley.newRequestQueue(context);
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {

                    ip = response.getString("ip");
                    callback.onSuccess(ip);

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }

        });

        queue.add(jsonObjectRequest);

        return null;

    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }
}

 /*   String ip = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet("http://ip2country.sourceforge.net/ip2c.php?format=JSON");
            HttpResponse response = httpclient.execute(httpget);
            Log.d("andy", "response=" + response);

            HttpEntity entity = response.getEntity();
            entity.getContentLength();
            String str = EntityUtils.toString(entity);
            // Toast.makeText(getApplicationContext(), str,
            // Toast.LENGTH_LONG).show();
            JSONObject json_data = new JSONObject(str);
            ip = json_data.getString("ip");
            // Toast.makeText(getApplicationContext(), ip,
            // Toast.LENGTH_LONG).show();
        } catch (Exception e) {

            e.printStackTrace();
        }
        Log.d("utils ip return", ip + "");
        return ip;*/