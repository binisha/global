package com.newitventure.mobile.GlobalHimalayan;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.newitventure.mobile.GlobalHimalayan.adapter.ListViewAdapter;
import com.newitventure.mobile.GlobalHimalayan.entity.CustomComparator;
import com.newitventure.mobile.GlobalHimalayan.parser.JSONParserYoutube;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class YoutubePlayActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {


    YouTubePlayer youTubePlayer;
    private static String GOOGLE_API_KEY = "AIzaSyCuO_7-soNoGNbbc9lSJI1pdLPx8Znt0K0";//this one is the browser key
    private static final String TAG_CATEGORY = "items";
    ArrayList<CustomComparator> compArrList;
    ArrayList<HashMap<String, String>> arraylist1;
    String categoryTitle, playlistId;
    private static JSONArray jsonarray, categories;
    private static JSONObject jsonobject;
    private static ProgressDialog dialogForGrid;

    boolean onListSelectDataTrueNot;
    int categorySize;
    ListViewAdapter adapter, adapter1;
    ListView playlist_list;
    YouTubePlayerView youtubePlayer;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        arraylist1 = new ArrayList<HashMap<String, String>>();
        compArrList = new ArrayList<CustomComparator>();

        playlist_list = (ListView) findViewById(R.id.playlist);
        TextView currentTitle = (TextView) findViewById(R.id.current_title);
        youtubePlayer = (YouTubePlayerView) findViewById(R.id.youtube_view);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        //this is animation to move text from right to left
       /* Animation animationToLeft = new TranslateAnimation(400, -400, 0, 0);
        animationToLeft.setDuration(12000);
        animationToLeft.setRepeatMode(Animation.RESTART);
        animationToLeft.setRepeatCount(Animation.INFINITE);
*/
        playlistId = getIntent().getStringExtra("playlist_id");
        categorySize = getIntent().getIntExtra("category_size", 0);
        categoryTitle = getIntent().getStringExtra("Category_title");


        currentTitle.setText(categoryTitle);

        new playlistAsynTask().execute();

        youtubePlayer.initialize(GOOGLE_API_KEY, YoutubePlayActivity.this);

    }


    public class playlistAsynTask extends AsyncTask<String, Void, String> {

        private CustomDialogManager loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub


            loadCategoryContent();
            return null;

        }

        protected void onPostExecute(String result) {
            progressBar.setVisibility(View.GONE);

        }
    }


    private void loadCategoryContent() {

        if (NetworkUtil.isInternetAvailable(YoutubePlayActivity.this)) {

            if (categorySize > 0) {
                try {

                    JSONParserYoutube categoryParser = new JSONParserYoutube();
                    String jsonUrl = "https://www.googleapis.com/youtube/v3/playlistItems?key="
                            + GOOGLE_API_KEY
                            + "&playlistId="
                            + playlistId
                            + "&part="
                            + "snippet,contentDetails&maxResults=50&fields=items(etag,id,snippet/title,snippet/position,snippet/publishedAt,snippet/description,snippet/thumbnails/default/url,snippet/resourceId/videoId),nextPageToken&pageToken=";
                    JSONObject category = categoryParser
                            .getJsonFromYoutube(jsonUrl);
                    jsonarray = category.getJSONArray("items");
                    String nextPage = "";
                    String idCollection = "";

                    do {
                        for (int i = 0; i < jsonarray.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();
                            jsonobject = jsonarray.getJSONObject(i);
                            JSONObject jO = jsonobject;// jsonobject.getJSONObject("url");

                            if (jO.getJSONObject("snippet").has("thumbnails")) {

                                String thisId = jO.getJSONObject("snippet")
                                        .getJSONObject("resourceId")
                                        .getString("videoId");
                                if (idCollection.equals("")) {
                                    idCollection = new StringBuilder()
                                            .append(idCollection).append(thisId)
                                            .toString();
                                } else {
                                    idCollection = new StringBuilder()
                                            .append(idCollection).append(",")
                                            .append(thisId).toString();
                                }
                                map.put("url", thisId);
                                map.put("thumb",
                                        jO.getJSONObject("snippet")
                                                .getJSONObject("thumbnails")
                                                .getJSONObject("default")
                                                .getString("url"));
                                map.put("title", jO.getJSONObject("snippet")
                                        .getString("title"));
                                String date = jO.getJSONObject("snippet")
                                        .getString("publishedAt");
                                map.put("date", date.substring(0, 10));
                                map.put("position", jO.getJSONObject("snippet")
                                        .getString("position"));
                                map.put("playlistId", playlistId);
                                map.put("youtube", "1");

                                arraylist1.add(map);
                            }


                        }

                        // now for each of the videos we have collected ids,
                        // following request for their duration.
                        String newJsonUrl = "https://www.googleapis.com/youtube/v3/videos?key=" + GOOGLE_API_KEY + "&id="
                                + idCollection
                                + "&part=contentDetails&fields=items(contentDetails/duration)";
                        JSONObject newCategory = categoryParser
                                .getJsonFromYoutube(newJsonUrl);
                        JSONArray jsonarray2 = newCategory.getJSONArray("items");
                        for (int i = 0; i < jsonarray2.length(); i++) {
                            HashMap<String, String> map2 = arraylist1.get(i);
                            JSONObject jsonobject2 = jsonarray2.getJSONObject(i);
                            JSONObject jO = jsonobject2;// jsonobject.getJSONObject("url");
                            String duration = jO.getJSONObject("contentDetails")
                                    .getString("duration");
                            String temp1 = duration.substring(2,
                                    duration.length() - 1);
                            if (temp1.contains("H")) {
                                String[] temp2 = temp1.split("H");
                                String[] temp3 = temp2[1].split("M");
                                if (temp2[0].length() == 1) {
                                    temp2[0] = "0" + temp2[0];
                                }
                                if (temp3[0].length() == 1) {
                                    temp3[0] = "0" + temp3[0];
                                }
                                if (temp3[1].length() == 1) {
                                    temp3[1] = "0" + temp3[1];
                                }
                                map2.put("duration", temp2[0] + ":" + temp3[0]
                                        + "." + temp3[1]);
                            } else {
                                if (temp1.contains("M")) {
                                    String[] temp4 = temp1.split("M");
                                    if (temp4[0].length() == 1) {
                                        temp4[0] = "0" + temp4[0];
                                    }
                                    if (temp4[1].length() == 1) {
                                        temp4[1] = "0" + temp4[1];
                                    }
                                    map2.put("duration", temp4[0] + "." + temp4[1]);
                                } else {
                                    if (temp1.length() == 1) {
                                        temp1 = "00.0" + temp1;
                                    } else {
                                        temp1 = "00." + temp1;
                                    }
                                    map2.put("duration", temp1);
                                }
                            }

                            String compUrl = map2.get("url");
                            String compThumb = map2.get("thumb");
                            String compTitle = map2.get("title");
                            String compDate = map2.get("date");
                            String compPosition = map2.get("position");
                            String compPlaylistId = map2.get("playlistId");
                            String compYoutube = map2.get("youtube");
                            String compDuration = map2.get("duration");

                            compArrList.add(new CustomComparator(compUrl,
                                    compThumb, compTitle, compDate, compPosition,
                                    compPlaylistId, compYoutube, compDuration));
                            arraylist1.set(i, map2);
                        }
                        if (category.has("nextPageToken")) {

                            nextPage = category.getString("nextPageToken");
                            String jsonUrl2 = new StringBuilder().append(jsonUrl)
                                    .append(nextPage).toString();
                            category = categoryParser.getJsonFromYoutube(jsonUrl2);
                            categories = category.getJSONArray(TAG_CATEGORY);

                        } else {

                            nextPage = "";

                        }

                    } while (nextPage != "");

                    onListSelectDataTrueNot = true;

                    if (arraylist1.size() > 1) {

                        onListSelectDataTrueNot = true;

                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                    onListSelectDataTrueNot = false;

                } catch (Exception e) {

                    e.printStackTrace();
                    onListSelectDataTrueNot = false;
                }

            }

            if (arraylist1.size() == 0) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // stuff that updates ui
                        Toast.makeText(YoutubePlayActivity.this,
                                "No videos in this category", Toast.LENGTH_LONG)
                                .show();
                    }

                });

            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    // stuff that updates ui

                    try {

                        Collections.sort(compArrList, new CustomComparator());
                        adapter1 = new ListViewAdapter(YoutubePlayActivity.this, compArrList);
                        playlist_list.setAdapter(adapter1);

                        youTubePlayer.loadVideo(compArrList.get(0).getUrl());

                        playlist_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                // Toast.makeText(YoutubePlayActivity.this,compArrList.get(position).getUrl() , Toast.LENGTH_SHORT).show();

                                youTubePlayer.loadVideo(compArrList.get(position).getUrl());


                            }
                        });


                        if (dialogForGrid != null) {

                            dialogForGrid.dismiss();
                            dialogForGrid = null;

                        }

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            });

        }

        // Binds the Adapter to the ListView
        // listview.setAdapter(adapter);

    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        if (!b) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically _oEA18Y8gM0
            // youTubePlayer.loadVideo(videoId);

            this.youTubePlayer = youTubePlayer;
            //youTubePlayer.loadVideo(compArrList.get(0).getUrl());
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            youTubePlayer.setShowFullscreenButton(true);
            youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION | YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);


        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {

        if (errorReason.isUserRecoverableError()) {

            errorReason.getErrorDialog(this, 1).show();

        } else {

            Toast.makeText(this, "errorMessage", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

}
