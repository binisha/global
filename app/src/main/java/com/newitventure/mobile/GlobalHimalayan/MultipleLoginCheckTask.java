package com.newitventure.mobile.GlobalHimalayan;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.newitventure.mobile.GlobalHimalayan.parser.JSONParser;
import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;

import org.json.JSONObject;

/**
 * Created by Dell on 02-Sep-15.
 */
public class MultipleLoginCheckTask extends AsyncTask<String,Void,String>{
    private Activity activity;
    private BroadcastReceiver br;

    String multilogin = null;

    public MultipleLoginCheckTask( Activity activity, BroadcastReceiver br ) {
        this.activity = activity;
        this.br = br;
    }

    @Override
    protected String doInBackground(String... str) {
        Log.e("Multiple Login Check TASK", str[0].replace(" ", "%20"));
        JSONParser jp = new JSONParser();
        try{
            JSONObject jsonobj = jp.getJSONFromUrl(str[0].replace(" ", "%20"));
            multilogin = jsonobj.getString("multilogin");
        }catch(Exception e){
            e.printStackTrace();
        }
        return multilogin;

    }

    @Override
    protected void onPostExecute( String result ) {
        Log.e("result", result + "");
        if (NetworkUtil.isInternetAvailable(activity)) {

            try{

                if (result.equals("yes")) {

                    try {
                        activity.unregisterReceiver(br);

                        Intent intent = new Intent(activity, DoubleLoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.e("should not go there", "OK I'll try not to");
                }

            }catch(Exception e){

                e.printStackTrace();
            }

        }
    }

}

