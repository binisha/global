package com.newitventure.mobile.GlobalHimalayan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.FacebookSdk;

import java.util.ArrayList;
import java.util.Arrays;

public class Settings extends AppCompatActivity {
    String[] settings = new String[]{" Logout", " Privacy Policy", " Terms of Service", " Contact Us", " About Us"};
    public static SharedPreferences app_preferences;
    Toolbar mtoolbar;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_settings);
        mtoolbar = (Toolbar) findViewById(R.id.settings_toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        textView = (TextView) findViewById(R.id.toolbar_title);
        textView.setText("Settings");

        ListView settingListView = (ListView) findViewById(R.id.settings_list);

        ArrayList<String> settingsArray = new ArrayList<String>();
        settingsArray.addAll(Arrays.asList(settings));

        ArrayAdapter<String> settingsListAdapter = new ArrayAdapter<String>(Settings.this, R.layout.list_row, R.id.rowTextView, settingsArray);
        settingListView.setAdapter(settingsListAdapter);

        settingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:

                        app_preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                        SharedPreferences.Editor editor = app_preferences.edit();
                        editor.clear();
                        editor.apply();

                        Intent intent = new Intent(Settings.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        finish();
                        break;
                    case 1:
                        Intent intent1 = new Intent(Settings.this, PrivacyPolicy.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(Settings.this, TermsofService.class);
                        startActivity(intent2);
                        break;
                    case 3:
                        Intent intent3 = new Intent(Settings.this, Contact.class);
                        startActivity(intent3);
                        break;
                    case 4:
                        Intent intent4 = new Intent(Settings.this, AboutUs.class);
                        startActivity(intent4);
                        break;


                }


            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
