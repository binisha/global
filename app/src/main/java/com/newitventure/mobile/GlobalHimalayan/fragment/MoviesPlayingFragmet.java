package com.newitventure.mobile.GlobalHimalayan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.newitventure.mobile.GlobalHimalayan.MovieDetails;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategoryParent;

import java.util.ArrayList;


public class MoviesPlayingFragmet extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

   public static ArrayList<MovieCategoryParent> movieCategoryParentList;


    private TextView txtMovieTitle , txtMovieDescription ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.movie_details_tab, container, false);


        txtMovieTitle = (TextView) v.findViewById(R.id.movieTitle);
        txtMovieDescription = (TextView) v.findViewById(R.id.movieDescription);
        Log.d("movie name", MovieDetails.currentMovieName+"");

        txtMovieTitle.setText(MovieDetails.currentMovieName);
        txtMovieDescription.setText(MovieDetails.currentMovieDescription);

        return v;
    }


}
