package com.newitventure.mobile.GlobalHimalayan.parser;

import android.content.Context;
import android.util.Log;

import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategoryParent;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * gets all the list of the category name and the description for the movies detail activity
 */
public class MovieCategoryParentParser {
	
	private static final String TAG = "com.newitventure.smartvision.movies.util.MovieCategoryParentParser";
	
	private static final String PARENT_ID = "parent_id";
	private static final String CATEGORY_NAME = "category_name";
	private static final String CATEGORY_DESC = "category_desc";
	private static final String CATEGORY_IMAGE = "category_image";
	private static final String RECOMMENDED_GROUP = "recommend_group";
	private static final String STATUS = "status";
	
	private String jsonString;
	public static ArrayList<MovieCategoryParent> movieParentCategoryList, movieParentCatlist;
	private Context context;
	
	public MovieCategoryParentParser(String jsonString, Context context) {
		this.jsonString = jsonString;
		this.context = context;
		
		movieParentCategoryList = new ArrayList<MovieCategoryParent>();
		movieParentCatlist = new ArrayList<MovieCategoryParent>();
	}
	
	public void parse() {
		try {
			JSONObject root = new JSONObject( jsonString );
			
			JSONArray categoryItems = root.getJSONArray( "category" );
			
			for( int i=0; i < categoryItems.length(); i++ ) {
				JSONObject item = categoryItems.getJSONObject( i );
				
				MovieCategoryParent parent = new MovieCategoryParent();
				
				parent.setParentId( item.getInt(PARENT_ID) );
				parent.setCategoryName( item.getString(CATEGORY_NAME) );
				parent.setCategoryDescription( item.getString(CATEGORY_DESC) );
				parent.setCategoryImageLink( item.getString(CATEGORY_IMAGE) );
				parent.setRecommendedGroup( item.getString(RECOMMENDED_GROUP) );
				parent.setStaus( item.getInt(STATUS) );
				
				try {
					UrlImageViewHelper.loadUrlDrawable( context, item.getString(CATEGORY_IMAGE) );
				}catch( Exception e ) {
					Log.e(TAG, "Could not pre-load drawable " + item.getString(CATEGORY_IMAGE));
				}
				movieParentCategoryList.add( parent );
				movieParentCatlist.add(parent);
			}
		}catch( Exception e ) {
			e.printStackTrace();
			
			//Log.e( TAG, e.getMessage() );
		}
	}
	
	public ArrayList<MovieCategoryParent> getMovieCategoryParentList() {
		return this.movieParentCategoryList;
	}
}
