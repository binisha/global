package com.newitventure.mobile.GlobalHimalayan.entity;


public class Item {

    String title;
    int res;


    public Item(String title, int res){
        this.title = title;
        this.res = res;

    }
    public String getTitle(){

        return this.title;
    }

    public int getRes(){

        return this.res;
    }
}
