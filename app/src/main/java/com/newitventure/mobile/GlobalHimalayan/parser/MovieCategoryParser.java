package com.newitventure.mobile.GlobalHimalayan.parser;

import android.util.Log;


import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * gets all the categories name of the movies
 */
public class MovieCategoryParser {

	private static final String TAG = "com.newitventure.smartvision.movies.util.MovieCategoryParser";
	
	private static final String CATEGORY_JSON_NAME = "name";
	private static final String CATEGORY_JSON_ID = "id";
	
	private String jsonString;
	public static ArrayList<MovieCategory> movieCategories;
	
	public MovieCategoryParser(String jsonString) {
		this.jsonString = jsonString;  
		movieCategories = new ArrayList<MovieCategory>();
	}
	
	public void parse() {
		
		movieCategories = new ArrayList<MovieCategory>();
		
		try {
			JSONObject root = new JSONObject( jsonString );
			
			JSONArray categoryItems = root.getJSONArray( "movie_categories" );
			
			for( int i=0; i < categoryItems.length(); i++ ) {
				JSONObject item = categoryItems.getJSONObject( i );
				
				//Log.d( TAG, item.toString() );
				MovieCategory movieCategory = new MovieCategory( item.getString( CATEGORY_JSON_NAME ), item.getInt( CATEGORY_JSON_ID ) );
				
				movieCategories.add( movieCategory );
				
				//Log.d( TAG, movieCategories.toString() );
			}
		}catch( Exception e ) {
			Log.e(TAG, e.getMessage());
		}
	}
	
	public ArrayList<MovieCategory> getMovieCategories() {
		return movieCategories;
	}
}
