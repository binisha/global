package com.newitventure.mobile.GlobalHimalayan.parser;

import android.content.Context;
import android.util.Log;


import com.newitventure.mobile.GlobalHimalayan.entity.Movie;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategory;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * gets all the genres/subcategories and movies inside the main category
 * stores them in a variable
 */
public class MovieParser {
	private static final String TAG = "com.newitventure.smartvision.movies.util.MovieParser";
	
	protected String jsonString;
	protected ArrayList<MovieCategory> movieCategories;
	protected Context context;
	
	private HashMap<String, ArrayList<Movie>> genreAndContent;
	private ArrayList<String> genreHavingContent;
	private ArrayList<String> genreNotHavingContent;
	
	public MovieParser(String jsonString, Context context) {
		this.jsonString = jsonString;
		this.context = context;
		
		
	}
	
	public void setMovieCategories( ArrayList<MovieCategory> movieCategories ) {
		this.movieCategories = movieCategories;
	}
	
	
	public void parse() {
		try {
			genreAndContent = new HashMap<String, ArrayList<Movie>>();
			genreHavingContent = new ArrayList<String>();
			genreNotHavingContent = new ArrayList<String>();
			
			JSONObject root = new JSONObject( jsonString );
			
			JSONObject moviesNode = root.getJSONObject( "movie_categories");
			//@SuppressWarnings("unchecked") // using legacy API
			//Iterator<String> keys = moviesNode.keys();
			
//			for(int k = movieCategories.size()-1;k>-1;k--){
			for( int k = 0; k < movieCategories.size(); k++ ) {
				MovieCategory movieCategory = movieCategories.get( k );
				
				int key = movieCategory.getCategoryId();
				
				if( moviesNode.has( key + "" ) ) {
					
					//Log.d( TAG, key + " found" );
					ArrayList<Movie> moviesUnderGenre = new ArrayList<Movie>();
					
					JSONArray itemsArray = moviesNode.getJSONArray( key + "" );
					
					for( int i=0; i < itemsArray.length(); i++ ) {
						JSONObject item = itemsArray.getJSONObject(i);
						Movie movie = this.getMovie(item);
						
						/*movie.setMovieId( item.getInt( "movie_id" ) );
						movie.setMovieName( item.getString( "movie_name" ) );
						movie.setMovieDescription( item.getString( "movie_desc" ) );
						movie.setMovieCategoryParentId( item.getInt( "movie_cat_parent" ) );
						movie.setMovieCategoryId( item.getInt( "movie_category_id" ) );
						movie.setMovieCategoryTitle( item.getString( "category_title" ) );
						movie.setMovieUrlString( item.getString( "movie_url" ) );
						movie.setMovieServerType( item.getInt( "movie_server_type" ) );
						movie.setMoviePictureLinkString( item.getString( "movie_picture" ) );
						movie.setMovieStatus( item.getInt( "movie_status" ) );
						movie.setMoviePrice( item.getDouble( "movie_price" ) );
						*/
						
						if( movie != null ) {
							moviesUnderGenre.add( movie );
						
//							UrlImageViewHelper.loadUrlDrawable(context, item.getString( "movie_picture" ) );
							//Log.d( TAG, movie.toString() );
						}
					}
					
					genreAndContent.put( movieCategory.getCategoryName(), moviesUnderGenre );
					genreHavingContent.add( movieCategory.getCategoryName() );
				}else {
					//Log.d( TAG, key + " not found" );
					
					genreNotHavingContent.add( movieCategory.getCategoryName() );
				}
			}
			
		}catch( Exception e ) {
			Log.e(TAG, e.getMessage());
		}
	}
	
	protected Movie getMovie( JSONObject item ) {
		Movie movie = null;
		
		try {
			movie = new Movie();
			
			movie.setMovieId( item.getInt( "movie_id" ) );
			movie.setMovieName( item.getString( "movie_name" ) );
			movie.setMovieDescription( item.getString( "movie_desc" ) );
			movie.setMovieCategoryParentId( item.getInt( "movie_cat_parent" ) );
			movie.setMovieCategoryId( item.getInt( "movie_category_id" ) );
//			movie.setMovieCategoryTitle( item.getString( "category_title" ) );
//			movie.setMovieUrlString( item.getString( "movie_url" ) );
			movie.setMoviePictureLinkString( item.getString( "movie_picture" ) );
			movie.setMovieStatus( item.getInt( "movie_status" ) );
		}catch( Exception e ) {
			Log.e(TAG, e.getMessage());
		}
		
		return movie;
	}
	
	public HashMap<String, ArrayList<Movie>> getContent() {
		return this.genreAndContent;
	}
	
	public ArrayList<String> getGenreHavingContent() {
		return this.genreHavingContent;
	}
	
	public ArrayList<String> getGenreNotHavingContent() {
		return this.genreNotHavingContent;
	}
}
