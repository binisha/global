package com.newitventure.mobile.GlobalHimalayan.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.newitventure.mobile.GlobalHimalayan.MovieDetails;
import com.newitventure.mobile.GlobalHimalayan.NoInternetActivity;
import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.entity.Movie;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MovieLinkLoader extends AsyncTask<String, Void, String> {
	private static final String TAG = "com.newitventure.smartvision.movies.MoviePlayActivity.MovieLinkLoader";
	String tvUrl, serverType, decryptedUrl, data1;
	int currentMovieId;
	CustomDialogManager loadingDialog;
	private Bundle extra;
	private ArrayList<? extends Parcelable> movielist;
	private Context context;
	String media_url, genre;
	Movie movie ;
	SharedPreferences app_preferences;

	public MovieLinkLoader(Context context, Movie movie, int movieID) {
		app_preferences = PreferenceManager.getDefaultSharedPreferences(context);

		String userid = app_preferences.getString("userid", "");

		Log.d(genre, genre + "we are here");
		this.context = context;
		this.movie = movie;
		this.currentMovieId = movieID;
//		http://internationaltv.sabhkuchh.com/json/movie/getmovie_json.php?movieID=504&userId=3&utc=1440315033&hash=74b9883da66d1479117b45a390e2d863
//		media_url = "http://internationaltv.sabhkuchh.com/json/movie/getmovie_json.php?movieID=" + currentMovieId+"&userId="+ userid+"&";
		genre = movie.getMovieCategoryTitle();

		media_url = LinkConfig.getString(context, LinkConfig.GETMOVIE_JSON)
				+ "?movieID=" + currentMovieId+"&userId="+ userid+"&";
	}

	@Override
	protected void onPreExecute() {

		super.onPreExecute();
		String message = context.getString(R.string.txt_loading);
		loadingDialog = new CustomDialogManager(context, message, CustomDialogManager.LOADING);
		loadingDialog.build();
		loadingDialog.show();

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		Log.d(TAG, media_url);

//		DownloadUtil dUtil = new DownloadUtil(media_url, context);
//
//		return dUtil.downloadStringContent();
		DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, context);
		String utc = getUtc.downloadStringContent();
		if (!utc.equals(DownloadUtil.NotOnline)
				&& !utc.equals(DownloadUtil.ServerUnrechable)) {

			DownloadUtil dUtil = new DownloadUtil(media_url
					+ LinkConfig.getHashCode(utc), context);
			Log.d(TAG + "link", media_url + LinkConfig.getHashCode(utc));
			return dUtil.downloadStringContent();
		} else
			return utc;
	}

	@Override
	protected void onPostExecute(String result) {

		if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
			Log.d(TAG, "internet not available");
			Intent intent = new Intent(context, NoInternetActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);

			// server not rechable
		} else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
			Log.d(TAG, "server_error");
			Intent i = new Intent(context, UnauthorizedAccess.class);
			i.putExtra("error_code", "error");
			i.putExtra("error_message", "");
			i.putExtra("username", "");
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(i);

		} else {

			Log.d(TAG + ".MovieLinkLoader", "reached async task");

			try {
				JSONObject root = new JSONObject(result);

				tvUrl = root.getString("link");
				serverType = root.getString("servertype");

				if (serverType.equals("1")) {
					// Decryption DOne here
					Mcrypt mCrypt = new Mcrypt();

					try {
						decryptedUrl = new String(mCrypt.decrypt(tvUrl));
						data1 = decryptedUrl.trim();
					} catch (Exception e) {
						Log.d(TAG, e.getMessage());
					}

				} else {
					data1 = tvUrl;
				}

				Log.d("DATA1 ! KO VALUE at movielink loader", data1 + genre
						+ "check");
				loadingDialog.dismiss();

				Intent i = new Intent(context, MovieDetails.class);
				i.putExtra("final_video_url", data1);
//				i.putExtra("movie_id", currentMovieId);
//				i.putExtra("genre_title", genre);
				i.putExtra("movie_name", movie.getMovieName());
				i.putExtra("movie_description", movie.getMovieDescription());
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// progressDialog.dismiss();
				context.startActivity(i);
			} catch (JSONException e) {
				Log.e(TAG, "JSON Exception " + e.getMessage());
				final CustomDialogManager alert = new CustomDialogManager(context, CustomDialogManager.ALERT);
				alert.build();
				alert.setMessage("Could not fetch the required data");
				alert.show();
				alert.setNeutralButton("Dismiss",
						new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								alert.dismiss();

							}
						});
			} catch (Exception e) {
				Log.e(TAG, "Exceptiopn " + e.getMessage());
				final CustomDialogManager alert = new CustomDialogManager(context, CustomDialogManager.ALERT);
				alert.build();
				alert.setMessage("Could not fetch the required data");
				alert.show();
				alert.setNeutralButton("Dismiss",
						new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								alert.dismiss();

							}
						});
			}
		}
	}

	private void setMovieLink(String jsonString) {

	}

	private void initMoviePlay() {

	}


}
