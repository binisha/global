package com.newitventure.mobile.GlobalHimalayan.adapter;

/**
 * Created by Dell on 18-Aug-15.
 */

        import android.content.Context;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.newitventure.mobile.GlobalHimalayan.R;
        import com.newitventure.mobile.GlobalHimalayan.entity.Channel;
        import com.squareup.picasso.Picasso;

        import java.util.ArrayList;


public class ChannelListAdapter extends ArrayAdapter<Channel> {

    private Context context;
    private ArrayList<Channel> channelList;
    private int rowLayoutResourceId;

    public ChannelListAdapter(Context context, int rowLayoutResourceId,
                              ArrayList<Channel> channelList) {
        super(context, rowLayoutResourceId, channelList);

        this.context = context;
        this.channelList = channelList;
        this.rowLayoutResourceId = rowLayoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if( convertView == null ) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            convertView = inflater.inflate( rowLayoutResourceId, null);

            holder = new ViewHolder();

            holder.channelNameTextView = (TextView) convertView.findViewById( R.id.channel_name );
//            holder.channelLanguageTextView = (TextView) convertView.findViewById( R.id.channel_language );
//            holder.channelNumberTextView = (TextView) convertView.findViewById( R.id.channel_no );
            holder.channelLogoImageView = (ImageView) convertView.findViewById( R.id.channel_logo );

            convertView.setTag( holder );
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Channel currentChannel = channelList.get( position );

        holder.channelNameTextView.setText( currentChannel.getChannelName() );

//        holder.channelNumberTextView.setText( currentChannel.getChannelPriority() + "" );

        String Link = currentChannel.getChannelLogoLink();

        Picasso.with(context)
                .load(Link)
                .into(holder.channelLogoImageView);

        Log.d(context.getClass().getName(), context.getClass().getSimpleName());

        return convertView;
    }

    static class ViewHolder {
        ImageView channelFavImageView;
        TextView channelNameTextView;
        TextView channelLanguageTextView;
        TextView channelNumberTextView;
        ImageView channelLogoImageView;
    }
}

