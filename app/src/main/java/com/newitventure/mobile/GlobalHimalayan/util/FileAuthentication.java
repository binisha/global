package com.newitventure.mobile.GlobalHimalayan.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.os.Environment;
import android.util.Log;

/*
 * 
 * 
 */
public class FileAuthentication {

    private static String userName, userId, userEmail, userPassword,sessionId;
    public static final String APP_CONFIG_FILE_NAME = "internationaltv_mylogin";
    // private static Boolean check;

    public static String getUserName() {
        return userName;
    }
    public static String getUserPassword(){
        return userPassword;
    }
    public static String getUserEmail(){
        return userEmail;
    }
    public static String getUserId() {
        return userId;
    }
    public static String getSessionId() {
        Log.d("session id here is",""+sessionId);
        return sessionId;
    }


    public static boolean reWriteLoginDetailsToFile(
                                                    String userEmail, String userPassword,  String sessionId) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {

            String st =  userEmail + "\n"	+ userPassword + "\n" + sessionId ;

            File externalStorageDir = Environment.getExternalStorageDirectory();
            File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME);

            try {
                FileOutputStream fOut1 = new FileOutputStream(myFile);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut1);
                myOutWriter.append(st);
                myOutWriter.close();
                fOut1.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        } else {
            return false;
        }

    }
    public static void checkFileExists() {
        File externalStorageDir = Environment.getExternalStorageDirectory();
        File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME );
        if (myFile.exists()) {
            myFile.delete();
        }

    }

    public static void LoginButtonFileCreate(String macId, String username, String email,  String password, String id) {
        File externalStorageDir = Environment.getExternalStorageDirectory();
        File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME );
        if (!myFile.exists()) {
            // Do action

            try {
                FileOutputStream fOut1 = new FileOutputStream(myFile);
                OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut1);
                myOutWriter.append(macId + "\n" + username + "\n"+ email + "\n"+password + "\n" + id);
                myOutWriter.close();
                fOut1.close();
            } catch (Exception e) {

            }
        }
        // Toast.makeText(MainActivity.this, "Not present",
        // Toast.LENGTH_SHORT).show();

    }

    @SuppressWarnings("resource")
    public static void setUserNameandId() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            // Toast.makeText(MainActivity.this,"Mounted",
            // Toast.LENGTH_SHORT).show();
            File externalStorageDir = Environment.getExternalStorageDirectory();
            File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME );

            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        new FileInputStream(myFile)));
                String data;
                StringBuffer sb = new StringBuffer();
                while ((data = br.readLine()) != null) {
                    sb.append(data + "\n");
                }

                // Toast.makeText(MainActivity.this,sb.toString(),
                // Toast.LENGTH_SHORT).show();
                String[] both = sb.toString().split("\n", 5);
//				macId = both[0].toString();
                userName = both[1].toString();
                userEmail = both[2].toString();
                userPassword = both[3].toString();
                userId = both[4].toString();
                // Toast.makeText(MainActivity.this,both[0].toString(),
                // Toast.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @SuppressWarnings("resource")
    public static Boolean checkMountAndFile(String macId) {
        // Toast.makeText(MainActivity.this,"Ok", Toast.LENGTH_SHORT).show();
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            // Toast.makeText(MainActivity.this,"Mounted",
            // Toast.LENGTH_SHORT).show();
            File externalStorageDir = Environment.getExternalStorageDirectory();
            File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME );
            if (myFile.exists()) {
                try {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(new FileInputStream(myFile)));
                    String data;
                    StringBuffer sb = new StringBuffer();
                    while ((data = br.readLine()) != null) {
                        sb.append(data + "\n");
                    }

                    // Toast.makeText(MainActivity.this,sb.toString(),
                    // Toast.LENGTH_SHORT).show();
                    String[] both = sb.toString().split("\n", 5);
                    if (both[0].toString().equals(macId)) {
//						macId = both[0].toString();
                        userName = both[1].toString();
                        userEmail = both[2].toString();
                        userPassword = both[3].toString();
                        userId = both[4].toString();
                        return true;
                    }
                    // Toast.makeText(MainActivity.this,both[0].toString(),
                    // Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;

    }

    // to create file while login
    public static void rewriteUserInfo(String macId, String username, String email, String password, String id) {
        File externalStorageDir = Environment.getExternalStorageDirectory();
        File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME );

        if (myFile.exists())
            myFile.delete();

        try {
            FileOutputStream fOut1 = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut1);
            myOutWriter.append(macId + "\n" + username + "\n"+ email+ "\n"+ password + "\n"+ id);
            myOutWriter.close();
            fOut1.close();
        } catch (Exception e) {

        }
    }


    public static boolean readFromFile(String macAddress) {

        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File externalStorageDir = Environment.getExternalStorageDirectory();
            File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME);
            if (myFile.exists()) {

                try {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(new FileInputStream(myFile)));

                    String data;
                    StringBuffer sb = new StringBuffer();
                    while ((data = br.readLine()) != null) {
                        sb.append(data + ",");
                    }
                    String str = sb.toString().replace(",,", "," );
                    String[] both = str.split(",");
                    if (both[0].toString().equals(macAddress)) {
                        userEmail = both[1].toString();
                        userPassword = both[2].toString();
                        sessionId = both[3].toString();

                        return true;
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return false;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return false;

        } else
            return false;
    }
}
