package com.newitventure.mobile.GlobalHimalayan.parser;

import android.content.Context;
import android.util.Log;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.newitventure.mobile.GlobalHimalayan.adapter.YouTubeCategoriesAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class YoutubeCategoriesParentParser {
private static final String TAG = "com.newitventure.GlobalHimalayan.youtube.util.YouTubeParentParser";

	
	private JSONArray jsonArray;
	private ArrayList<YouTubeCategoriesAdapter> youtubeParentCategoryList;
	private Context context;
	
	public YoutubeCategoriesParentParser(JSONArray result, Context context) {
		this.jsonArray = result;
		this.context = context;
		
		youtubeParentCategoryList = new ArrayList<YouTubeCategoriesAdapter>();
	}
	
	public void parse() {
		try {				
			for( int i=0; i < jsonArray.length(); i++ ) {
				JSONObject item = jsonArray.getJSONObject( i );
				
				if(item.getString("title")!= null && !item.getString("title").isEmpty()){
				YouTubeCategoriesAdapter parent = new YouTubeCategoriesAdapter();
				parent.setParentId(item.getString("user_id"));
				parent.setCategoryName( item.getString("title"));
				parent.setCategoryDescription(item.getString("channel_description"));
				parent.setCategoryImageLink( item.getString("youtube_picture"));				
				try {
					UrlImageViewHelper.loadUrlDrawable(context, item.getString("youtube_picture"));
				}catch( Exception e ) {
					Log.e( TAG, "Could not pre-load drawable " + item.getString("youtube_picture") );
				}
				youtubeParentCategoryList.add(parent);
				}
			}
		}catch( Exception e ) {
			e.printStackTrace();

		}
	}
	
	public ArrayList<YouTubeCategoriesAdapter> getMovieCategoryParentList() {
		return this.youtubeParentCategoryList;
	}

}
