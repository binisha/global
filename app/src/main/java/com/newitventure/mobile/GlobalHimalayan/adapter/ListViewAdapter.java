package com.newitventure.mobile.GlobalHimalayan.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.newitventure.mobile.GlobalHimalayan.entity.CustomComparator;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;
import com.newitventure.mobile.GlobalHimalayan.R;


import java.util.ArrayList;


public class ListViewAdapter extends BaseAdapter {
 
	 // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<CustomComparator> data;
    //ImageLoaderG imageLoader;
 
    public ListViewAdapter(Context context,
                           ArrayList<CustomComparator> compArrList) {
        this.context = context;
        data = compArrList;
       // imageLoader = new ImageLoaderG(context);
 
    }
 
    @Override
    public int getCount() {
        return data.size();
    }
 
    @Override
    public Object getItem(int position) {
        return null;
    }
 
    @Override
    public long getItemId(int position) {
        return 0;
    }


    private class ViewHolder {

        TextView title, date, duration;
        ImageView thumb;
    }
    @Override
	public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            convertView = inflater.inflate( R.layout.grid_list_item_vod_youtube, null);

            holder = new ViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.gridItemText);
            holder.date = (TextView) convertView.findViewById(R.id.gridItemTextDate);
            holder.duration = (TextView) convertView.findViewById(R.id.gridItemTextDuration);
            holder.thumb = (ImageView) convertView.findViewById(R.id.gridItemImage);


            convertView.setTag(holder);
        }
        else {

            holder = (ViewHolder) convertView.getTag();
        }


        CustomComparator resultp =data.get(position);

        holder.title.setText(Html.fromHtml(resultp.getTitle()));
        holder.date.setText("Date: " + resultp.getDate());
        holder.duration.setText("Duration: " + resultp.getDuration());
            // Capture position and set results to the ImageView
            // Passes flag images URL into ImageLoader.class to download and cache
            // images

        try {
            UrlImageViewHelper.setUrlDrawable(holder.thumb,
                    resultp.getThumb(),
                    R.drawable.placeholder_logo, LinkConfig.CACHE_HOLD_DURATION);

        } catch (Exception e) {

            e.printStackTrace();
            holder.thumb.setImageDrawable(context.getResources()
                    .getDrawable(R.drawable.placeholder_logo));
        }

        return convertView;
    }
}