package com.newitventure.mobile.GlobalHimalayan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;

import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.newitventure.mobile.GlobalHimalayan.R;


public class Splashscreen extends ActionBarActivity {


    public static SharedPreferences app_preferences;
    SharedPreferences.Editor editor;
    public static boolean fbsignedIn;
    public static boolean opsignedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);



        Thread thread = new Thread() {
            public void run() {
                try {

                    sleep(3000);

                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally {

                    if(NetworkUtil.isInternetAvailable(Splashscreen.this)) {
                        app_preferences = PreferenceManager.getDefaultSharedPreferences(Splashscreen.this);
                        editor  = app_preferences.edit();
                        fbsignedIn = app_preferences.getBoolean("fbsignedin", false);
                        opsignedIn = app_preferences.getBoolean("opsignedin",false);
                        if (fbsignedIn||opsignedIn) {

                            Intent intent = new Intent(Splashscreen.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            Intent intent = new Intent(Splashscreen.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        finish();
                    }


                }

            }




        };
        thread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }
}
