package com.newitventure.mobile.GlobalHimalayan;


import android.app.AlertDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.adapter.ViewPagerAdapterMovieDetails;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategoryParent;
import com.newitventure.mobile.GlobalHimalayan.parser.MovieCategoryParentParser;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.CustomViewPager;
import com.newitventure.mobile.GlobalHimalayan.util.MoviesVideoControllerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;


public class MovieDetails extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MoviesVideoControllerView.MediaPlayerControl {

    Toolbar toolbar;
    public static String currentMovieName, currentMovieDescription;
    public String movieUrl;
    private CustomViewPager pager;
    private SlidingTabLayout slidingTabs;
    private ViewPagerAdapterMovieDetails adapter;
    ImageButton playButton;
    ProgressBar tvProgressBar;
    private FrameLayout videoContainer;
    private SurfaceView videoSurface;
    private RelativeLayout errorLayout;
    private ImageView retry;
    private TextView errorMsg, serverUnreachableError;
    public static MediaPlayer player;
    private View decorView;
    private RelativeLayout mainContainer;
    public static MoviesVideoControllerView controller;
    private AlertDialog alertDialog;
    AdView mAdView;
    CustomDialogManager loadingDialog;
    private boolean mFullScreen;
    private static final String TAG = "MovieDetails";
    public static ArrayList<MovieCategoryParent> movieCategoryParentList;
    int numOfTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movie_details);

        decorView = getWindow().getDecorView();

        toolbar = (Toolbar) findViewById(R.id.movieDetailsToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pager = (CustomViewPager) findViewById(R.id.movies_viewpager);

        videoContainer = (FrameLayout) findViewById(R.id.video_surface_container);
        videoSurface = (SurfaceView) findViewById(R.id.video_surface);
        playButton = (ImageButton) findViewById(R.id.play_button);

        tvProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        errorLayout = (RelativeLayout) findViewById(R.id.network_error_layout);
        serverUnreachableError = (TextView) findViewById(R.id.server_unreachable_error_msg);
        errorMsg = (TextView) findViewById(R.id.network_error_msg);
        retry = (ImageView) findViewById(R.id.network_retry);

        slidingTabs = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabs.setDistributeEvenly(true);
        slidingTabs.setBackgroundColor(getResources().getColor(R.color.primary));
        slidingTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));

        mAdView = (AdView) findViewById(R.id.adView);

        getCurrentOrientation();
        loadData();

        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                playButton.setVisibility(View.GONE);
                tvProgressBar.setVisibility(View.VISIBLE);
                loadStreamVideo(movieUrl);

            }

        });

        retry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                errorMsg.setVisibility(View.GONE);
                retry.setVisibility(View.GONE);
                tvProgressBar.setVisibility(View.VISIBLE);

                Handler mHand = new Handler();
                mHand.postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        loadStreamVideo(movieUrl);

                    }

                }, 1000);

            }

        });

        videoSurface.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (controller.isShowing()) {

                    controller.hide();

                } else {

                    controller.show();

                }

                return false;

            }

        });

    }


    private void getCurrentOrientation() {

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            setFullscreenLayout();

        } else {

            setPortraitLayout();

        }

    }


    public void loadData() {

        currentMovieName = getIntent().getStringExtra("movie_name");
        currentMovieDescription = getIntent().getStringExtra("movie_description");
        // loadStreamVideo(getIntent().getStringExtra("final_video_url"));

        movieUrl = getIntent().getStringExtra("final_video_url");

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        SurfaceHolder videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);

        player = new MediaPlayer();
        controller = new MoviesVideoControllerView(MovieDetails.this,false);

        getSupportActionBar().setTitle(getIntent().getStringExtra("movie_name"));

        MovieCategoryParent ma = new MovieCategoryParent();
        ma.setCategoryName("MOVIE");

        movieCategoryParentList = MovieCategoryParentParser.movieParentCatlist;
        movieCategoryParentList.remove(0);
        movieCategoryParentList.add(0, ma);

        Log.e("MOVIER CATEGORY", movieCategoryParentList + "" + movieCategoryParentList.size());
        numOfTabs = movieCategoryParentList.size();

        adapter = new ViewPagerAdapterMovieDetails(getSupportFragmentManager(), movieCategoryParentList, numOfTabs);
        pager.setAdapter(adapter);
        slidingTabs.setViewPager(pager);

    }


    public void loadStreamVideo(String link) {

        try {

            player.setOnPreparedListener(this);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(this, Uri.parse(link));
            player.setOnErrorListener(MovieDetails.this);

            try {

                player.prepareAsync();

            } catch (Exception e) {

                e.printStackTrace();

            }

        } catch (IllegalArgumentException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

            serverUnreachableError.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        player.setDisplay(holder);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {


    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    /**
     * Prepares the media player to play the video
     */
    @Override
    public void onPrepared(MediaPlayer mp) {

        controller.setMediaPlayer(this);
        controller.setAnchorView((FrameLayout) findViewById(R.id.video_surface_container));
        player.start();
//        controller.setProgress();

        tvProgressBar.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);

    }

    /**
     * Catches the all the errors related with the media player
     */
    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {

        try {

            player.reset();
            controller.hide();
            tvProgressBar.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            errorMsg.setVisibility(View.VISIBLE);
            retry.setVisibility(View.VISIBLE);


        } catch (Exception e) {

            e.printStackTrace();

        }

        return false;

    }

    @Override
    public void start() {

        player.start();

    }

    @Override
    public void pause() {

        player.pause();

    }

    @Override
    public int getDuration() {

        return player.getDuration();

    }

    @Override
    public int getCurrentPosition() {

        return player.getCurrentPosition();

    }

    @Override
    public void seekTo(int pos) {

        player.seekTo(pos);

    }

    @Override
    public boolean isPlaying() {

        return player.isPlaying();

    }

    @Override
    public int getBufferPercentage() {

        return 0;

    }

    @Override
    public boolean canPause() {

        return true;

    }

    @Override
    public boolean canSeekBackward() {

        return true;

    }

    @Override
    public boolean canSeekForward() {

        return true;

    }



    @Override
    public boolean isFullScreen() {

        return mFullScreen;

    }


    /**
     * Sets the video player to the Landscape mode or portrait mode on clicking the toogle button
     */
    @Override
    public void toggleFullScreen() {

        if (isFullScreen()) {

            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        } else {

            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

            setFullscreenLayout();

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            setPortraitLayout();

        }

    }


    /**
     * Sets the layout when the video player is set to the full screen mode
     */
    private void setFullscreenLayout() {

        this.getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //adLinLay.setVisibility(View.GONE);

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoContainer.getLayoutParams();

        //params.addRule( RelativeLayout.ALIGN_PARENT_TOP & RelativeLayout.ALIGN_PARENT_LEFT & RelativeLayout.ALIGN_PARENT_RIGHT & RelativeLayout.ALIGN_PARENT_BOTTOM );

        params.height = metrics.heightPixels;
        params.width = metrics.widthPixels;// + getTitlebarHeight();
        params.rightMargin = 0;
        params.bottomMargin = 0;
//        params.setMargins(0, (getTitlebarHeight() * metrics.heightPixels / metrics.widthPixels / 2), 0, 0);
        params.setMargins(0, 0, 0, 0);
        videoContainer.setLayoutParams(params);
        mFullScreen = true;

    }


    /**
     * Sets the layout when the video player is set to the portrait mode
     */
    private void setPortraitLayout() {

        this.getSupportActionBar().show();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //adLinLay.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;

        //The size of the diagonal in inches is equal to the square root of the height in inches squared plus the width in inches squared.
        double diagonalInches = Math.sqrt(
                (widthInches * widthInches)
                        + (heightInches * heightInches));

        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoContainer.getLayoutParams();

        if (diagonalInches >= 10) {

            params.width = metrics.widthPixels;
            params.height = (int) (440 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        } else if (diagonalInches >= 7) {

            params.width = metrics.widthPixels;
            params.height = (int) (540 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        } else {

            params.width = metrics.widthPixels;
            params.height = (int) (240 * metrics.density);
            params.setMargins(0, 0, 0, 0);

        }

        videoContainer.setLayoutParams(params);
        mFullScreen = false;

    }

    @Override
    public void onResume() {

        super.onResume();

        playButton.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        errorMsg.setVisibility(View.GONE);
        retry.setVisibility(View.GONE);

    }

    @Override
    public void onPause() {

        super.onPause();

        if (player.isPlaying()) {

            player.stop();
            player.reset();

        }

    }

    @Override
    public void onDestroy() {

        super.onDestroy();

        controller.hide();

        try {

            if (player.isPlaying()) {

                player.stop();
                player.release();

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

}






/* private void loadStreamVideo(String streamLink) {
        try {

            videoView.setVideoURI(Uri.parse(streamLink));
            videoView.setMediaController(controller);
            // videoView.setMediaController( null );

            videoView.requestFocus();
            // mVideoView.start();

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {

                        @Override
                        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                            controller.setAnchorView(videoView);

                        }
                    });

                    videoView.start();
                    // alertDialog.dismiss();
                    loadingDialog.dismiss();
                }
            });

            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

                @Override
                public boolean onError(MediaPlayer mp, int what,
                                       int extra) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
                            MovieDetails.this);
                    dialogBuilder.setMessage("Error loading video").setTitle(
                            "Error");

                    dialogBuilder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    if (dialog != null) {
                                        dialog.dismiss();
                                        //dismissDialog(dialog);
                                    }

                                    Intent returnIntent = new Intent();
                                    setResult(RESULT_CANCELED, returnIntent);
                                    finish();

                                }

                            });

                    AlertDialog dialog = dialogBuilder.create();

                    //alertDialog.dismiss();
                    dismissDialog(alertDialog);

                    dialog.show();

                    return true;
                }
            });

        } catch (Exception e) {
            // mProgressBar.setVisibility(View.INVISIBLE);
            // progressDialog.dismiss();
            // finish();
            e.printStackTrace();
        }
    }


    private void setFullscreenLayout() {

        // getActionBar().hide();
        pager.setVisibility(View.GONE);

        slidingTabs.setVisibility(View.GONE);
        toolbar.setVisibility(View.GONE);
        mAdView.setVisibility(View.GONE);


        //Remove notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mainContainer.setBackgroundColor(getResources().getColor(R.color.black_background));
        //mAdViewBottom.setVisibility(View.INVISIBLE);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();

        //params.addRule( RelativeLayout.ALIGN_PARENT_TOP & RelativeLayout.ALIGN_PARENT_LEFT & RelativeLayout.ALIGN_PARENT_RIGHT & RelativeLayout.ALIGN_PARENT_BOTTOM );

        params.height = metrics.heightPixels;
        params.width = metrics.widthPixels + getTitlebarHeight();


        params.setMargins(0, 0, 0, 0);

        videoView.setLayoutParams(params);
        controller.setVideoState(CustomMediaController.VIDEO_FULLSCREEN);
    }


    private void setPortraitLayout() {
        pager.setVisibility(View.VISIBLE);

        slidingTabs.setVisibility(View.VISIBLE);

        toolbar.setVisibility(View.VISIBLE);
        mAdView.setVisibility(View.VISIBLE);

        getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //mainContainer.setBackgroundColor( getResources().getColor( R.color.white_background ) );
        // mAdViewBottom.setVisibility(View.VISIBLE);

        //videoView.setLayoutParams(paramsNotFullscreen);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();

        params.width = metrics.widthPixels;
        params.height = (int) (200 * metrics.density);
        params.setMargins(0, 0, 0, 0);

        Log.i(TAG, params.height + " ");
        //params.addRule( RelativeLayout.ALIGN_PARENT_TOP | RelativeLayout.ALIGN_PARENT_LEFT  );

        //params.leftMargin = 30;
        videoView.setLayoutParams(params);
        controller.setVideoState(CustomMediaController.VIDEO_NOT_FULLSCREEN);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {

            setFullscreenLayout();


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            setPortraitLayout();

        }

    }

    public void toggleFullScreen(int videoState) {

        if (videoState == CustomMediaController.VIDEO_FULLSCREEN) {

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        } else {

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }

    }

    private int getTitlebarHeight() {

        Rect rectangle = new Rect();
        decorView.getWindowVisibleDisplayFrame(rectangle);

        int statusBarHeight = rectangle.top;
        int contentViewTop =
                getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();

        int titleBarHeight = contentViewTop - statusBarHeight;

        return titleBarHeight;

    }


    private void dismissDialog(final DialogInterface dialog) {

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {

            @Override
            public void run() {

                dialog.dismiss();

            }

        }, 3000);

    }*/