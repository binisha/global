package com.newitventure.mobile.GlobalHimalayan.util;


import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;

import com.newitventure.mobile.GlobalHimalayan.MovieDetails;
import com.newitventure.mobile.GlobalHimalayan.Player;
import com.newitventure.mobile.GlobalHimalayan.R;


public class CustomMediaController extends MediaController {

    public static final int VIDEO_FULLSCREEN = 1;
    public static final int VIDEO_NOT_FULLSCREEN = 0;

    public static final int PLAYER_MOVIES = 501;
    public static final int PLAYER_LIVE_TV = 502;

    private static final String TAG = "CustomMediaController";

    private Context context;
    private ImageButton customButton;

    private int videoState;
    private int player;

    public CustomMediaController(Context context, int player) {
        super( context );

        this.context = context;
        this.player = player;
        //this.videoActivity = videoActivity;

        videoState = VIDEO_NOT_FULLSCREEN;
    }

    @Override
    public void setAnchorView(View view) {
        super.setAnchorView(view);

        //FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams( 44, 44 );
        frameParams.gravity = Gravity.TOP | Gravity.RIGHT;
        frameParams.rightMargin = 10;
        frameParams.topMargin = 40;

        View customView = makeCustomView();

        addView( customView, frameParams );

    }

    private View makeCustomView() {
        customButton = new ImageButton( context );
        customButton.setBackgroundColor( context.getResources().getColor( android.R.color.transparent ) );
        customButton.setImageResource( R.drawable.fullscreen );

        customButton.setOnClickListener( new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Log.d(TAG, "should switch to fullscreen");

                videoState = 1 - videoState;

                if( player == PLAYER_LIVE_TV ) {
                   // ((Player)context).toggleFullScreen( videoState );
                }else {
					// ((MovieDetails)context).toggleFullScreen( videoState );
                }

                if( videoState == VIDEO_FULLSCREEN ) {
                    //set image to toggle back to normal screen
                    customButton.setImageResource( R.drawable.normal_screen );
                }else {
                    //set image to toggle to full screen
                    customButton.setImageResource( R.drawable.fullscreen );
                }
            }

        });

        return customButton;

    }

    public int mTimeout = 0;

    @Override
    public void show() {
        show(mTimeout);
    }

    @Override
    public void show(int timeout) {
        super.show(mTimeout);
    }

    @Override
    public void hide() {
        // Do not hide until a timeout is set
        if (mTimeout > 0) super.hide();
    }

    public void hideActually() {
        super.hide();
    }


    public void setVideoState( int state ) {
        videoState = state;

        if( videoState == VIDEO_FULLSCREEN ) {
            customButton.setImageResource( R.drawable.normal_screen );
        }else {
            customButton.setImageResource( R.drawable.fullscreen );
        }
    }
}
