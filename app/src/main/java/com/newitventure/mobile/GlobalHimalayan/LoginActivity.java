package com.newitventure.mobile.GlobalHimalayan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;
import com.newitventure.mobile.GlobalHimalayan.util.NetworkUtil;
import com.newitventure.mobile.GlobalHimalayan.util.SecurityUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


public class LoginActivity extends ActionBarActivity {
    ImageView fbimage;
    public static String encrypted_password, decrypted_password;

    String fbemail;
    JSONObject picture;
    private CallbackManager mcallbackManager;
    //    private Button fbloginbutton ,fblogout;
    private static final String TAG = "LoginActivity";
    String link;

    private boolean islogin = false;
    ;
    public static SharedPreferences app_preferences;
    public static boolean fbsignedIn;
    public static boolean opsignedIn;


    private TextView cancelText;
    private Button loginImageView;
    String uname, pword;
    //    public static String macAddress="acdbda259dcf";
    public static String validity, username, userId, email, userGroup;
    public static String display_name, sessionId;
    SharedPreferences.Editor editor;
    Button signUp;
    public TextInputLayout inputLayoutName, inputLayoutPassword;
    public EditText usernameView, passwordView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        app_preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        editor = app_preferences.edit();
        fbsignedIn = app_preferences.getBoolean("fbsignedin", false);
        opsignedIn = app_preferences.getBoolean("opsignedin", false);
        if (fbsignedIn || opsignedIn) {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        mcallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.input_layout_password);

        usernameView = (EditText) findViewById(R.id.usernameField);
        passwordView = (EditText) findViewById(R.id.passwordField);
        loginImageView = (Button) findViewById(R.id.login_Button);
        signUp = (Button) findViewById(R.id.signUP);
//        usernameView.setSelected(false);
        loginImageView.requestFocus();

        signUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, SignUpWebsite.class);
                startActivity(intent);

            }

        });

        loginImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                uname = usernameView.getText().toString().trim();
                pword = passwordView.getText().toString().trim();

                if (NetworkUtil.isInternetAvailable(LoginActivity.this)) {

                    if (uname.length() > 0 && pword.length() > 0) {

                        String URL = LinkConfig.getString(LoginActivity.this,
                                LinkConfig.LOGIN_BUTTON_CLICK)
                                + "?uname="
                                + uname
                                + "&pswd=" + pword;

                        SecurityUtils sUtils = new SecurityUtils();
                        new PerformTask(sUtils.getEncryptedToken(pword))
                                .execute(URL);
                    } else {

                        submitForm();

                    }

                }

            }

        });

    }

    private void submitForm() {

        if (!validateName()) {

            return;

        }

        if (!validatePassword()) {

            return;

        }

    }

    private boolean validateName() {

        if (uname.isEmpty()) {

            inputLayoutName.setError(getString(R.string.err_msg_username));
            requestFocus(usernameView);
            return false;

        } else {

            inputLayoutName.setErrorEnabled(false);

        }

        return true;
    }

    private boolean validatePassword() {

        if (pword.isEmpty()) {

            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(passwordView);
            return false;

        } else {

            inputLayoutPassword.setErrorEnabled(false);

        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void Loginwithfacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "fbemail"));
        LoginManager.getInstance().registerCallback(mcallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Profile profile = Profile.getCurrentProfile();
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,link,fbemail,picture");

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject user, GraphResponse graphResponse) {

                        Log.v(TAG, "the result is " + user.toString());
                        try {
                            fbemail = user.getString("fbemail");
                            picture = user.getJSONObject("picture");
                            JSONObject url = picture.getJSONObject("data");
                            link = url.getString("url");


                            Log.v(TAG, "the user fbemail is" + fbemail);
                            Log.v(TAG, "the user fbemail is" + picture);
                            Log.v(TAG, "the user profile link  is" + link);

                            editSignedinvalue();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                Log.d("you login is", "cancel");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d("some error occur", "failed");
            }
        });
    }

    private void editSignedinvalue() {

        fbsignedIn = true;
        editor.putBoolean("fbsignedin", fbsignedIn);
        editor.putString("fbemail", fbemail);
        editor.putString("link", link);
        editor.apply();

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mcallbackManager.onActivityResult(requestCode, resultCode, data);


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private class PerformTask extends AsyncTask<String, String, String> {


        public PerformTask(String encrypted_password) {
            encrypted_password = encrypted_password;
        }

        private CustomDialogManager loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String titleLoading = getString(R.string.loadingTitle_youtube);
            String message = getString(R.string.txt_loading);
            loadingDialog = new CustomDialogManager(LoginActivity.this,
                    CustomDialogManager.LOADING);
            loadingDialog.build();
            loadingDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {

            Log.d("the login url is", params[0]);
            DownloadUtil util = new DownloadUtil(params[0], LoginActivity.this);
            return util.downloadStringContent();

        }

        @Override
        protected void onPostExecute(String result) {

            loadingDialog.dismiss();
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                Log.d(TAG, "internet not available");
                Intent intent = new Intent(LoginActivity.this, NoInternetActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                LoginActivity.this.finish();
                // server not rechable
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray loginArray = jsonObject.getJSONArray("login");
                    Log.v(TAG, "the result(login) is" + loginArray);

                    JSONObject lnObect = loginArray.getJSONObject(0);
                    Log.v(TAG, "the 0 object is " + lnObect);

                    String loginstatus = lnObect.getString("is_active");
                    Log.v(TAG, "the login status  is" + loginstatus);


                    if (loginstatus.equals("1")) {

                        userId = lnObect.getString("user_id");
                        Log.d("test userid is", "user  " + userId);
                        username = lnObect.getString("display_name");
                        userGroup = lnObect.getString("user_group");

                        display_name = username;

                        opsignedIn = true;
                        editor.putBoolean("opsignedin", opsignedIn);
                        editor.putString("useremail", uname);
                        editor.putString("user_group", userGroup);
                        editor.putString("userid", userId);
                        editor.putString("username", username);
                        editor.apply();

                        new GetSession().execute(LinkConfig.getString(
                                LoginActivity.this, LinkConfig.GET_SESSION)
                                + "?"
                                + "uname="
                                + uname);

                    } else {

                        JSONObject jsonmessage = lnObect.getJSONObject("message");

                        String message = jsonmessage.getString("message_body");
                        Log.v(TAG, "the error message is" + message);

                        Toast toast = Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG);
                        View view = toast.getView();
                        view.setBackgroundResource(R.color.red);
                        toast.show();

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

        }

    }


    private class GetSession extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.e("GET SESSION URL", params[0] + "");
            DownloadUtil dutil = new DownloadUtil(params[0], LoginActivity.this);
            String json = dutil.downloadStringContent();
            return json;
        }

        @Override
        protected void onPostExecute(String result) {
            JSONObject jobject = null;
            try {
                jobject = new JSONObject(result);
                sessionId = jobject.getString("session");
                Log.d("session id is  ", "" + sessionId);

                editor.putString("sessionid", sessionId);
                editor.apply();


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();

        }
    }
}
