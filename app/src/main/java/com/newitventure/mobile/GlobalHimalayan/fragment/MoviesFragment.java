package com.newitventure.mobile.GlobalHimalayan.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newitventure.mobile.GlobalHimalayan.R;
import com.newitventure.mobile.GlobalHimalayan.Tabs.SlidingTabLayout;
import com.newitventure.mobile.GlobalHimalayan.adapter.ViewPagerAdapterMovies;
import com.newitventure.mobile.GlobalHimalayan.entity.MovieCategoryParent;
import com.newitventure.mobile.GlobalHimalayan.util.CustomDialogManager;
import com.newitventure.mobile.GlobalHimalayan.util.CustomViewPager;
import com.newitventure.mobile.GlobalHimalayan.util.DownloadUtil;
import com.newitventure.mobile.GlobalHimalayan.util.LinkConfig;
import com.newitventure.mobile.GlobalHimalayan.parser.MovieCategoryParentParser;

import java.util.ArrayList;

public class MoviesFragment extends Fragment {


    int numOfTabs;
    ArrayList<String> MovieCategoryTabTitles;
    ViewPagerAdapterMovies adapter;
    Context context;
    private SharedPreferences app_preferences;
    CustomViewPager pager;
    SlidingTabLayout slidingTabs;
    public static String userid, sessionid, userGroup;
    String TAG = "com.example.dell.WodApplication.fragment";
    public RelativeLayout relativeLayout;
    TextView textView;

    public static ArrayList<MovieCategoryParent> movieCategoryParentList;


    private OnFragmentInteractionListener mListener;


    public MoviesFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movies, container, false);

        pager = (CustomViewPager) v.findViewById(R.id.movies_viewpager);

        slidingTabs = (SlidingTabLayout) v.findViewById(R.id.sliding_tabs);

        textView = (TextView) getActivity().findViewById(R.id.toolbar_title);
        textView.setText("Movies");

        relativeLayout = (RelativeLayout) v.findViewById(R.id.relative_layout);

        app_preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userid = app_preferences.getString("userid", "");
        sessionid = app_preferences.getString("sessionid", "");
        userGroup = app_preferences.getString("user_group", "");

        String movieCategoryUrl = LinkConfig.getString(
                getActivity(), LinkConfig.MOVIE_PARENT_CATEGORY);

        movieCategoryUrl += "?groupId="
                + userGroup + "&userId=" + userid + "&";

        new MovieCategoryJsonParser().execute(movieCategoryUrl);

        return v;

    }

    public interface OnFragmentInteractionListener {

        public void onFragmentInteraction(Uri uri);

    }

    private class MovieCategoryJsonParser extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            relativeLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            Log.d("moviecategoryjson parser url", params[0]);
//			return dUtil.downloadStringContent();
            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, getActivity());
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {

                DownloadUtil dUtil = new DownloadUtil(params[0]
                        + LinkConfig.getHashCode(utc), getActivity());
                Log.d(TAG, params[0] + LinkConfig.getHashCode(utc));
                return dUtil.downloadStringContent();
            } else
                return utc;


        }

        protected void onPostExecute(String result) {

            try {

                if (result != null) {

                    final int highestPosition = result.length() - 1;

                    MovieCategoryParentParser parser = new MovieCategoryParentParser(
                            result, getActivity());
                    parser.parse();

                    movieCategoryParentList = parser.getMovieCategoryParentList();

                    Log.d("Highest Position", highestPosition + "");
                    numOfTabs = movieCategoryParentList.size();

                    relativeLayout.setVisibility(View.GONE);
                    adapter = new ViewPagerAdapterMovies(getChildFragmentManager(), movieCategoryParentList, numOfTabs);
                    pager.setAdapter(adapter);

                    slidingTabs.setDistributeEvenly(true);
                    slidingTabs.setBackgroundColor(getResources().getColor(R.color.primary));
                    slidingTabs.setCustomTabView(R.layout.custom_tab_view, R.id.tab_text);
                    slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.white));
                    slidingTabs.setViewPager(pager);

                }

            } catch (Exception e) {

                e.printStackTrace();

            }

        }

    }

}
